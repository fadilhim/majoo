import 'dart:io';

import 'package:chopper/chopper.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_stetho/flutter_stetho.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:majoo/bloc/auth/auth_cubit.dart';
import 'package:majoo/bloc/favorite/favorite_cubit.dart';
import 'package:majoo/bloc/people/people_cubit.dart';
import 'package:majoo/model/film.dart';
import 'package:majoo/model/people.dart';
import 'package:majoo/model/species.dart';
import 'package:majoo/my_app.dart';
import 'package:majoo/repositories/cache/preferences/user_cache.dart';
import 'package:majoo/repositories/favorite_repository.dart';
import 'package:majoo/repositories/network/film_service.dart';
import 'package:majoo/repositories/network/people_service.dart';
import 'package:majoo/repositories/network/service_manager.dart';
import 'package:majoo/repositories/network/species_service.dart';
import 'package:majoo/repositories/network/utilities/http_overrides.dart';
import 'package:majoo/repositories/network/utilities/json_serializable_converter.dart';
import 'package:majoo/repositories/people_repository.dart';
import 'package:majoo/repositories/session_repository.dart';
import 'package:provider/provider.dart';

final injector = GetIt.instance;
const namedService = 'service';
const namedAppName = 'majoo';

void main() async {
  Stetho.initialize();

  await _prepareFramework();
  await initializeDateFormatting('id_ID', null);

  runApp(MultiBlocProvider(
    providers: [
      BlocProvider(create: FavoriteCubit.create),
      BlocProvider(create: PeopleCubit.create),
      BlocProvider(create: AuthCubit.create),
    ],
    child: MyApp(),
  ));
}

Future<void> _prepareFramework() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  /// don't remove , for issue Provider value notifier .
  Provider.debugCheckInvalidValueType = null;

  if (!kIsWeb) {
    HttpOverrides.global = FrameworkHttpOverrides();
  }

  FrameworkComponent._beginConfigure();

  final _frameworkModule = FrameworkModule();

  await _frameworkModule.configureCache(injector);
  _frameworkModule.configureNetwork(injector);
  _frameworkModule.configureData(injector);

  FrameworkComponent._endConfigure();
}

class FrameworkComponent {
  FrameworkComponent._();

  static void _beginConfigure() {
    injector.registerSingleton<GlobalKey<NavigatorState>>(
        GlobalKey<NavigatorState>());
    injector.registerSingleton<List<ChopperService>>(<ChopperService>[],
        instanceName: namedService);
    injector.registerSingleton<Map<Type, JsonFactory>>(<Type, JsonFactory>{});
  }

  static void _endConfigure() {
    injector.registerLazySingleton<JsonSerializableConverter>(
        () => JsonSerializableConverter(injector.get()));
    injector.registerSingleton<ServiceManager>(ServiceManager.create(
      FrameworkEnvironment.dev().baseCoreEndPoint,
      injector.get(),
      injector.get(instanceName: namedService),
    ));
  }
}

class FrameworkEnvironment {
  final String baseCoreEndPoint;
  final String name;

  const FrameworkEnvironment({
    required this.name,
    required this.baseCoreEndPoint,
  });

  const FrameworkEnvironment.dev()
      : this(
          name: 'majoo',
          baseCoreEndPoint: 'https://swapi.dev/api',
        );
}

class FrameworkModule {
  const FrameworkModule() : super();

  void configureNetwork(GetIt injector) {
    injector.registerService(PeopleService.create);
    injector.registerService(FilmService.create);
    injector.registerService(SpeciesService.create);

    injector.get<Map<Type, JsonFactory>>().addAll({
      Map: (json) => json,
      People: People.fromJsonFactory,
      Film: Film.fromJsonFactory,
      Species: Species.fromJsonFactory,
    });
  }

  Future<void> configureCache(GetIt injector) async {
    injector.registerLazySingleton<UserCache>(UserCache.create);
    return;
  }

  void configureData(GetIt injector) {
    injector.registerLazySingleton<PeopleRepository>(
      () => PeopleRepository(injector.get()),
    );
    injector.registerLazySingleton<FavoriteRepository>(
      () => FavoriteRepository(),
    );
    injector.registerLazySingleton<SessionRepository>(
      () => SessionRepository(
        injector.get(),
        injector.get(),
        injector.get(),
        injector.get(),
      ),
    );
  }
}

extension GetItExtension on GetIt {
  void registerService<T extends ChopperService>(FactoryFunc<T> create,
      {String? instanceName}) {
    final T service = create();
    injector.get<List<ChopperService>>(instanceName: namedService).add(service);
    registerSingleton<T>(service);
  }
}
