import 'dart:io';

import 'package:majoo/repositories/network/repository_error.dart';

class ErrorMessage {
  static const errorCodeUnknownError = 0;
  static const errorCodeWithUnlocalizedMessage = 1;
  static const errorCodeNoInternet = 2;

  final String? unlocalizedMessage;
  final int errorCode;

  factory ErrorMessage(dynamic e) {
    if (e is RepositoryException) {
      return ErrorMessage._create(
          unlocalizedMessage: e.userMessage,
          errorCode: errorCodeWithUnlocalizedMessage);
    }
    if (e is SocketException) {
      return ErrorMessage._create(
          unlocalizedMessage: null, errorCode: errorCodeNoInternet);
    }
    return ErrorMessage._create(
      unlocalizedMessage: null,
    );
  }

  ErrorMessage._create({
    required this.unlocalizedMessage,
    this.errorCode = errorCodeUnknownError,
  });

  String getMessage() {
    switch (errorCode) {
      case errorCodeWithUnlocalizedMessage:
        return unlocalizedMessage ?? 'Oops somethings went wrong';
      case errorCodeNoInternet:
        return 'Unable to communicate to server, please check your connection';
      default:
        return 'Oops somethings went wrong';
    }
  }
}
