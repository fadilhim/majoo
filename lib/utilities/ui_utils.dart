class ResponsiveUi {
  static const smallWidth = 400.0;
  static const mediumWidth = 600.0;
  static const largeWidth = 1200.0;

  static const dialogMaxWidth = 350.0;
  static const dialogMaxHeight = 500.0;
  static const pageMaxWidth = 900.0;

  ResponsiveUi._();
}
