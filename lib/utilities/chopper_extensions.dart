import 'package:chopper/chopper.dart';
import 'package:majoo/repositories/network/repository_error.dart';

extension ResponseExtension<BodyType> on Response<BodyType> {
  BodyType get bodyNotNull {
    final _body = this.body;
    if (_body != null) {
      return _body;
    }
    throw RepositoryException(
      'Empty response',
      StackTrace.current,
    );
  }
}
