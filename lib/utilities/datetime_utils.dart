import 'package:intl/intl.dart';

final _formatYear = DateFormat('yyyy-MM-dd');
final _formatDay = DateFormat('E', 'id_ID');
final _formatFullDay = DateFormat('EEEEE, dd MMMM yyyy', 'id_ID');

extension DateTimeFormatUtils on DateTime {
  String get yearFormat => _formatYear.format(this);

  String get fullDayFormat => _formatFullDay.format(this);

  String get dayFormat => _formatDay.format(this);
}

extension StringTimeFormatUtils on String {
  DateTime get yearFormat => _formatYear.parse(this);

  DateTime get fullDayFormat => _formatFullDay.parse(this);

  DateTime get dayFormat => _formatDay.parse(this);
}
