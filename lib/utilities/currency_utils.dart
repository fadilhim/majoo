import 'package:intl/intl.dart';

final _formatCurrency = NumberFormat('#,###');

extension StringCurrencyFormatUtils on int {
  String get currencyFormat => _formatCurrency.format(this);
}