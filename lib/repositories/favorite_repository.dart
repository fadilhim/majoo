import 'dart:async';

import 'package:majoo/model/people.dart';
import 'package:majoo/repositories/cache/dao/favorite_dao.dart';

class FavoriteRepository {
  final _favoriteDao = FavoriteDao();
  final StreamController<List<String>> _streamController =
      StreamController.broadcast();
  final List<String> _favList = [];

  Future<List<String>> getAll({String? name}) =>
      _favoriteDao.getFavorites(name: name);

  Future<void> insert(People people) {
    _favoriteDao.createFavorite(people);

    _favList.add(people.id);

    _streamController.add(_list);
    return Future.value();
  }

  Future<void> deleteFavoriteById(String id) {
    _favoriteDao.deleteFavorite(id);

    _favList.removeWhere((element) => element == id);

    _streamController.add(_list);
    return Future.value();
  }

  Future deleteAll() {
    _favoriteDao.deleteAllFavorites();

    _list.clear();

    _streamController.add(_list);
    return Future.value();
  }

  Stream<List<String>> watchFavoriteList() => _streamController.stream;

  List<String> get _list => _favList;
}
