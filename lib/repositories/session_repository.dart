import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:majoo/main.dart';
import 'package:majoo/model/user.dart';
import 'package:majoo/pages/login/login_page.dart';
import 'package:majoo/repositories/cache/dao/user_dao.dart';
import 'package:majoo/repositories/favorite_repository.dart';
import 'package:majoo/repositories/network/repository_error.dart';
import 'package:majoo/repositories/people_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'cache/preferences/user_cache.dart';
import 'network/service_manager.dart';

class SessionRepository {
  final ServiceManager _serviceManager;
  final UserCache _userCache;
  final FavoriteRepository _favoriteRepository;
  final PeopleRepository _peopleRepository;
  final _userDao = UserDao();

  SessionRepository(
    this._userCache,
    this._serviceManager,
    this._favoriteRepository,
    this._peopleRepository,
  );

  Future<User> login({required String email, required String password}) async {
    final response = await _userDao.searchUser(email: email);

    if (response == null) {
      throw RepositoryException(
        'Empty User',
        StackTrace.current,
        userMessage: 'Akun belum tersedia, mohon daftarkan terlebih dahulu',
      );
    }

    final _user = response;
    if (_user.password != password) {
      throw RepositoryException(
        'Credentials Wrong',
        StackTrace.current,
        userMessage: 'Email atau password salah!',
      );
    }

    await _userCache.setUser(_user);

    await start();

    return _user;
  }

  Future<User> register({
    required String email,
    required String password,
    required String name,
    required int age,
    required int height,
    required int mass,
  }) async {
    final response = await _userDao.searchUser(email: email);

    if (response != null) {
      throw RepositoryException(
        'Registered User',
        StackTrace.current,
        userMessage:
            'Akun sudah pernah terdaftar, silahkan login atau daftar dengan email yg berbeda',
      );
    }

    final _user = User(
      email: email,
      name: name,
      password: password,
      age: age,
      height: height,
      mass: mass,
    );

    await _userDao.createUser(_user);
    return _user;
  }

  Future<User?> getLoggedUser() async => await _userCache.getLoggedUser();

  Future<void> logout() async => await stop();

  Future<void> forceLogout() async {
    await logout();
    final GlobalKey<NavigatorState> navigatorKey = injector.get();
    navigatorKey.currentState?.pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LoginPage()), (route) => false);
  }

  Future<bool> start() async {
    final user = await _userCache.getLoggedUser();
    return user != null;
  }

  Future<void> stop() async {
    _serviceManager.clear();
    final prefs = await SharedPreferences.getInstance();
    await prefs.clear();
    await _favoriteRepository.deleteAll();
    await _peopleRepository.deleteAll();
  }
}
