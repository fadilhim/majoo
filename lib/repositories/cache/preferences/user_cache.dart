import 'dart:convert';

import 'package:majoo/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserCache {

  static UserCache create() => UserCache._();

  UserCache._();

  setUser(User user) async {
    final _instance = await SharedPreferences.getInstance();
    _instance.setString('user', jsonEncode(user.toJson()));
  }

  Future<User?> getLoggedUser() async {
    final _instance = await SharedPreferences.getInstance();
    final _user = _instance.getString('user');
    if (_user != null ){
      final _u = jsonDecode(_user);
      return User.fromJson(_u);
    }
    return null;
  }

  Future<void> reset() async {
    final _instance = await SharedPreferences.getInstance();
    _instance.clear();
  }
}
