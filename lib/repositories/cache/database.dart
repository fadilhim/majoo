import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

final peopleTABLE = 'People';
final userTABLE = 'User';
final favoriteTABLE = 'Favorite';

class DatabaseProvider {
  static final DatabaseProvider dbProvider = DatabaseProvider();
  Database? _database;

  Future<Database> get database async {
    final _database = this._database;
    if (_database != null) {
      return _database;
    } else {
      final _dbCreate = await createDatabase();
      this._database = _dbCreate;
      return _dbCreate;
    }
  }

  createDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    //"ReactiveTodo.db is our database instance name
    String path = join(documentsDirectory.path, "Majoo.db");
    var database = await openDatabase(path,
        version: 1, onCreate: initDB);
    return database;
  }

  void initDB(Database database, int version) async {
    await database.execute("CREATE TABLE $userTABLE ("
        "id TEXT PRIMARY KEY, "
        "content TEXT, "
        "name TEXT"
        ")");
    await database.execute("CREATE TABLE $peopleTABLE ("
        "id TEXT PRIMARY KEY, "
        "content TEXT, "
        "name TEXT"
        ")");
    await database.execute("CREATE TABLE $favoriteTABLE ("
        "id TEXT PRIMARY KEY, "
        "name TEXT"
        ")");
  }
}
