import 'dart:async';
import 'dart:convert';

import 'package:majoo/model/db_type.dart';
import 'package:majoo/model/user.dart';
import 'package:majoo/repositories/cache/database.dart';

class UserDao {
  final dbProvider = DatabaseProvider.dbProvider;

  //Adds new User records
  Future<int> createUser(User user) async {
    final db = await dbProvider.database;
    final value = user.toJson();
    final dbType = DBType(id: user.email, content: jsonEncode(value), name: user.email);

    final result = db.insert(userTABLE, dbType.toJson());
    return result;
  }

  //Get All User items
  //Searches if query string was passed
  Future<List<User>> getUsers({List<String>? columns, String? email}) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result = [];
    if (email != null) {
      if (email.isNotEmpty)
        result = await db.query(userTABLE,
            columns: columns, where: 'name LIKE ?', whereArgs: ["%$email%"]);
    } else {
      result = await db.query(userTABLE, columns: columns);
    }

    List<User> users = result.isNotEmpty
        ? result.map((item) {
            final val = jsonDecode(item['content']);
            return User.fromJson(val);
          }).toList()
        : [];
    return users;
  }

  Future<User?> searchUser(
      {List<String>? columns, required String email}) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result = [];
    if (email.isNotEmpty)
      result = await db.query(
        userTABLE,
        columns: columns,
        where: 'name = ?',
        whereArgs: [email],
      );

    User? user;
    if (result.isNotEmpty) {
      final val = jsonDecode(result.first['content']);
      return User.fromJson(val);
    }
    return user;
  }
}
