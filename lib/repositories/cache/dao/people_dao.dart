import 'dart:async';
import 'dart:convert';

import 'package:majoo/model/db_type.dart';
import 'package:majoo/model/people.dart';
import 'package:majoo/repositories/cache/database.dart';

class PeopleDao {
  final dbProvider = DatabaseProvider.dbProvider;

  //Adds new People records
  Future<int> createPeople(People people) async {
    final db = await dbProvider.database;
    final value = people.toJson();
    final dbType =
        DBType(id: people.id, content: jsonEncode(value), name: people.name);

    final result = db.insert(peopleTABLE, dbType.toJson());
    return result;
  }

  //Get All People items
  //Searches if query string was passed
  Future<List<People>> getPeoples({List<String>? columns, String? name}) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result = [];
    if (name != null) {
      if (name.isNotEmpty)
        result = await db.query(peopleTABLE,
            columns: columns, where: 'name LIKE ?', whereArgs: ["%$name%"]);
    } else {
      result = await db.query(peopleTABLE, columns: columns);
    }

    List<People> peoples = result.isNotEmpty
        ? result.map((item) {
            final val = jsonDecode(item['content']);
            return People.fromJson(val);
          }).toList()
        : [];
    return peoples;
  }

  Future<int> updatePeople(People people) async {
    final db = await dbProvider.database;

    final value = people.toJson();
    final dbType =
        DBType(id: people.id, content: jsonEncode(value), name: people.name);

    var result = await db.update(peopleTABLE, dbType.toJson(),
        where: "id = ?", whereArgs: [people.id]);

    return result;
  }

  Future<int> deletePeople(String id) async {
    final db = await dbProvider.database;
    var result = await db.delete(peopleTABLE, where: 'id = ?', whereArgs: [id]);

    return result;
  }

  Future deleteAllPeoples() async {
    final db = await dbProvider.database;
    var result = await db.delete(
      peopleTABLE,
    );

    return result;
  }
}
