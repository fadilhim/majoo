import 'dart:async';
import 'dart:convert';

import 'package:majoo/model/db_type.dart';
import 'package:majoo/model/people.dart';
import 'package:majoo/repositories/cache/database.dart';

class FavoriteDao {
  final dbProvider = DatabaseProvider.dbProvider;

  //Adds new Favorite records
  Future<int> createFavorite(People people) async {
    final db = await dbProvider.database;

    final result =
        db.insert(favoriteTABLE, {'id': people.id, 'name': people.name});
    return result;
  }

  //Get All People items
  //Searches if query string was passed
  Future<List<String>> getFavorites(
      {List<String>? columns, String? name}) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result = [];
    if (name != null) {
      if (name.isNotEmpty)
        result = await db.query(favoriteTABLE,
            columns: columns, where: 'name LIKE ?', whereArgs: ["%$name%"]);
    } else {
      result = await db.query(favoriteTABLE, columns: columns);
    }

    List<String> peoples =
        result.isNotEmpty ? result.map((item) => item['id'] as String).toList() : [];
    return peoples;
  }

  Future<int> deleteFavorite(String id) async {
    final db = await dbProvider.database;
    var result = await db.delete(favoriteTABLE, where: 'id = ?', whereArgs: [id]);

    return result;
  }

  Future deleteAllFavorites() async {
    final db = await dbProvider.database;
    var result = await db.delete(
      favoriteTABLE,
    );

    return result;
  }
}
