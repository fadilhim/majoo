import 'package:majoo/model/people.dart';
import 'package:majoo/repositories/cache/dao/people_dao.dart';
import 'package:majoo/repositories/network/people_service.dart';
import 'package:majoo/utilities/chopper_extensions.dart';

class PeopleRepository {
  final _peopleDao = PeopleDao();
  final PeopleService _peopleService;

  PeopleRepository(this._peopleService);

  Future<List<People>> getAllLocal({String? name}) =>
      _peopleDao.getPeoples(name: name);

  Future<List<People>> getFromAPI({int? page}) async {
    final result = await _peopleService.getListPeople(page: page);

    return result.bodyNotNull;
  }

  Future insert(People people) => _peopleDao.createPeople(people);

  Future insertAll(List<People> peoples) {
    for (final people in peoples) {
      _peopleDao.createPeople(people);
    }

    return Future.value();
  }

  Future update(People people) => _peopleDao.updatePeople(people);

  Future deletePeopleById(String id) => _peopleDao.deletePeople(id);

  Future deleteAll() => _peopleDao.deleteAllPeoples();
}
