import 'package:chopper/chopper.dart';
import 'package:majoo/repositories/network/repository_error.dart';
import 'package:majoo/repositories/network/response/error_response.dart';

typedef JsonFactory<T> = T Function(Map<String, dynamic> json);

class JsonSerializableConverter extends JsonConverter {
  final Map<Type, JsonFactory> factories;

  const JsonSerializableConverter(this.factories);

  T? _decodeMap<T>(Map<String, dynamic> values) {
    final jsonFactory = factories[T];
    if (jsonFactory == null || jsonFactory is! JsonFactory<T>) {
      return null;
    }
    try {
      return jsonFactory(values);
    } catch (error, stackTrace) {
      throw InvalidJsonException(error.toString(), stackTrace);
    }
  }

  List<T> _decodeList<T>(Iterable values) =>
      values.where((v) => v != null).map<T>((v) => _decode<T>(v)).toList();

  dynamic _decode<T>(dynamic entity) {
    if (entity is Map<String, dynamic>) {
      if (entity.containsKey('results')) {
        return _decodeList<T>(entity['results']);
      } else {
        final decodedMap = _decodeMap<T>(entity);
        if (decodedMap != null) {
          return decodedMap;
        }
      }
    }
    return entity;
  }

  @override
  Response<ResultType> convertResponse<ResultType, Item>(Response response) {
    final jsonRes = super.convertResponse(response);

    return jsonRes.copyWith<ResultType>(body: _decode<Item>(jsonRes.body));
  }

  @override
  Response convertError<ResultType, Item>(Response response) {
    final jsonRes = super.convertError(response);

    if (jsonRes.body is! Map) {
      throw InvalidJsonException(
          'Invalid error response format', StackTrace.current);
    }

    final mapBody = <String, dynamic>{'body': jsonRes.body};
    final errorResponse = jsonRes.copyWith<ErrorResponse>(
      body: ErrorResponse.fromJsonFactory(mapBody),
    );
    return errorResponse;
  }
}
