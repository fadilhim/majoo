import 'dart:async';

import 'package:chopper/chopper.dart' as chopper;
import 'package:majoo/repositories/network/network_error.dart';
import 'package:majoo/repositories/network/response/error_response.dart';

class ErrorInterceptor implements chopper.ResponseInterceptor {
  const ErrorInterceptor();

  @override
  FutureOr<chopper.Response> onResponse(chopper.Response response) async {
    if (!response.isSuccessful && response.error is ErrorResponse) {
      final error = response.error;
      if (error is ErrorResponse) {
        final message = error.body['errors']?.first['message'] ?? error.body['message'];
        throw NetworkException(
          message,
          StackTrace.current,
          payload: error.body,
          userMessage: message,
        );
      }
    }
    return response;
  }
}
