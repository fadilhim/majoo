import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
// ignore: implementation_imports, package doesn't provide in main file
import 'package:flutter_stetho/src/http_client.dart';

class FrameworkHttpOverrides extends HttpOverrides {
  static FrameworkHttpOverrides? _instance;

  factory FrameworkHttpOverrides() {
    if (!kIsWeb && kDebugMode && Platform.isAndroid) {
      WidgetsFlutterBinding.ensureInitialized();
    }
    return _instance ??= FrameworkHttpOverrides._();
  }

  FrameworkHttpOverrides._();

  @override
  HttpClient createHttpClient(SecurityContext? context) {
    final client = super.createHttpClient(context);
    if (kDebugMode && Platform.isAndroid) {
      return StethoHttpClient(client: client);
    }
    return client;
  }

}
