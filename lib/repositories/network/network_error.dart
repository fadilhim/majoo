
import 'package:majoo/repositories/network/repository_error.dart';

class NetworkException extends RepositoryException {
  final Map<String, dynamic>? payload;

  NetworkException(
      String message,
      StackTrace stackTrace, {
        this.payload,
        String? userMessage,
      }) : super(
    message,
    stackTrace,
    userMessage: userMessage,
  );
}
