// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'species_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$SpeciesService extends SpeciesService {
  _$SpeciesService([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = SpeciesService;

  @override
  Future<Response<Species>> getSpecies({required String id}) {
    final $url = '/species/$id';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<Species, Species>($request);
  }
}
