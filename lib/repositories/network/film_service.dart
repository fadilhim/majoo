import 'dart:async';

import 'package:chopper/chopper.dart';
import 'package:majoo/model/film.dart';

part 'film_service.chopper.dart';

@ChopperApi(baseUrl: '/films')
abstract class FilmService extends ChopperService {
  static FilmService create([ChopperClient? client]) =>
      _$FilmService(client);

  @Get(path: '/{id}')
  Future<Response<Film>> getFilm({@Path() required String id});
}
