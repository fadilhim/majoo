import 'dart:async';

import 'package:chopper/chopper.dart';
import 'package:majoo/repositories/network/utilities/error_interceptor.dart';
import 'package:majoo/repositories/network/utilities/json_serializable_converter.dart';

class ServiceManager {
  late ChopperClient client;

  String? _baseCoreEndpoint;

  static ServiceManager create(
      String baseCoreEndpoint,
      JsonSerializableConverter converter,
      List<ChopperService> service,
      ) =>
      ServiceManager._(baseCoreEndpoint, converter, service);

  ServiceManager._(String baseCoreEndpoint, JsonSerializableConverter converter,
      List<ChopperService> service) {
    Future<Request> headerInterceptor(Request request) async {
      final headers = Map<String, String>.of(request.headers);

      return request.copyWith(headers: headers);
    }

    client = ChopperClient(
      converter: converter,
      interceptors: [
        headerInterceptor,
            (Request request) {
          final _baseCoreEndpoint = this._baseCoreEndpoint;
          if (_baseCoreEndpoint != null && _baseCoreEndpoint.isNotEmpty) {
            return request.copyWith(baseUrl: _baseCoreEndpoint);
          }
          return request;
        },
        const ErrorInterceptor(),
      ],
      errorConverter: converter,
      baseUrl: baseCoreEndpoint,
      services: service,
    );
  }

  Iterable createInterceptor() => [];

  void setConnectionConfig(String? baseCoreEndpoint) {
    _baseCoreEndpoint = baseCoreEndpoint;
  }

  void clear() {
    _baseCoreEndpoint = null;
  }
}
