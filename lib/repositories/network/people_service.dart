import 'dart:async';

import 'package:chopper/chopper.dart';
import 'package:majoo/model/people.dart';

part 'people_service.chopper.dart';

@ChopperApi(baseUrl: '/people')
abstract class PeopleService extends ChopperService {
  static PeopleService create([ChopperClient? client]) =>
      _$PeopleService(client);

  @Get()
  Future<Response<List<People>>> getListPeople({@Query('page') int? page});
}
