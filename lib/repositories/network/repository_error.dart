import 'dart:io';

const _invalidJSONExceptionMessage =
    'Unclear connection from server, please contact our customer support';
const _defaultMessage = 'Oops somethings went wrong';

class RepositoryException implements IOException {
  final String message;
  final StackTrace stackTrace;
  final int? errorCode;
  final String userMessage;

  RepositoryException(
    this.message,
    this.stackTrace, {
    this.errorCode,
    String? userMessage,
  }) : userMessage = userMessage ?? _defaultMessage;

  @override
  String toString() => 'RepositoryException: $message, errorCode : $errorCode';
}

class InvalidJsonException extends RepositoryException {
  InvalidJsonException(
    String message,
    StackTrace stackTrace,
  ) : super(
          message,
          stackTrace,
          userMessage: _invalidJSONExceptionMessage,
        );
}
