// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'film_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$FilmService extends FilmService {
  _$FilmService([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = FilmService;

  @override
  Future<Response<Film>> getFilm({required String id}) {
    final $url = '/films/$id';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<Film, Film>($request);
  }
}
