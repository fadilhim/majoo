import 'dart:async';

import 'package:chopper/chopper.dart';
import 'package:majoo/model/species.dart';

part 'species_service.chopper.dart';

@ChopperApi(baseUrl: '/species')
abstract class SpeciesService extends ChopperService {
  static SpeciesService create([ChopperClient? client]) =>
      _$SpeciesService(client);

  @Get(path: '/{id}')
  Future<Response<Species>> getSpecies({@Path() required String id});
}
