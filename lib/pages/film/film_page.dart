import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo/bloc/film/film_cubit.dart';
import 'package:majoo/model/film.dart';
import 'package:majoo/widgets/custom_appbar.dart';
import 'package:majoo/utilities/datetime_utils.dart';
import 'package:majoo/widgets/dialog/error_dialog.dart';
import 'package:majoo/widgets/loading_widget.dart';

class DetailFilmPage extends StatefulWidget {
  final String filmId;

  DetailFilmPage(this.filmId);

  @override
  _DetailFilmPageState createState() => _DetailFilmPageState();
}

class _DetailFilmPageState extends State<DetailFilmPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF8F8F8),
      appBar: CustomAppBar(title: 'Detail Film'),
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 9, horizontal: 12),
          child: _body(),
        ),
      ),
    );
  }

  Widget _body() {
    return BlocProvider<FilmCubit>(
      create: (context) => FilmCubit.create(context, widget.filmId),
      child: BlocConsumer<FilmCubit, FilmState>(
        listener: (context, state) {
          if (state is FilmLoadFailure) {
            ErrorDialog.showDialog(
              context,
              message: state.error.getMessage(),
            );
          }
        },
        builder: (context, state) {
          if (state is FilmLoadInProgress) {
            return Expanded(child: Center(child: LoadingWidget()));
          } else if (state is FilmLoadSuccess) {
            return SafeArea(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 9, horizontal: 12),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Image.network(
                          "https://fiverr-res.cloudinary.com/images/t_main1,q_auto,f_auto,q_auto,f_auto/gigs/206523336/original/f4649ba811b9d926ea712381d2bf5cf95a7feaa7/do-a-landscape-minimalist-movie-poster-and-artwork.jpg",
                          height: 100,
                        ),
                      ),
                      SizedBox(height: 9),
                      Divider(
                        color: Colors.black12,
                      ),
                      SizedBox(height: 9),
                      ..._detailContainer(state.film)
                    ],
                  ),
                ),
              ),
            );
          }
          return Expanded(
              child: Center(
                  child: ErrorWidget(
                      'Sepertinya sedang ada gangguan di server kami, mohon kembali nanti')));
        },
      ),
    );
  }

  List<Widget> _detailContainer(Film film) {
    return [
      featureContainer(
        'Title',
        film.title,
        Icons.drive_file_rename_outline,
      ),
      featureContainer(
        'Director',
        film.director,
        Icons.face,
      ),
      featureContainer(
        'Epidose ID',
        film.episodeId.toString(),
        Icons.timer_sharp,
      ),
      featureContainer(
        'Release Date',
        film.releaseDate.fullDayFormat,
        Icons.date_range,
      ),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 4),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(children: [
              Padding(
                padding: EdgeInsets.only(right: 20),
                child: Icon(Icons.description, size: 25),
              ),
              Text('Opening Crawl'),
            ]),
            SizedBox(height: 8),
            Text(
              film.openingCrawl,
              style: TextStyle(
                fontSize: 14.5,
                color: Colors.black54,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
    ];
  }

  Widget featureContainer(String title, String description, IconData icon) {
    return InkWell(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: 20),
                      child: Icon(icon, size: 25),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: Text(
                        title,
                        style: TextStyle(
                          fontSize: 14.5,
                          color: Colors.black54,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: Text(description),
                )
              ],
            ),
          ),
          Divider(
            color: Colors.black12,
          )
        ],
      ),
    );
  }
}
