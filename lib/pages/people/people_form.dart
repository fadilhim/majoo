import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:majoo/model/people.dart';
import 'package:majoo/widgets/form_field/text_form_field.dart';

class PeopleForm extends StatefulWidget {
  final GlobalKey<FormState> formKey;
  final PeopleController controller;

  const PeopleForm({required this.formKey, required this.controller, Key? key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PeopleForm();
  }
}

class _PeopleForm extends State<PeopleForm> {
  final _nameController = TextController();
  final _heightController = TextController();
  final _massController = TextController();
  final _hairColorController = TextController();
  final _skinColorController = TextController();
  final _eyeColorController = TextController();
  final _birthYearController = TextController();

  @override
  void initState() {
    super.initState();
    _bind(widget.controller.value);
  }

  _bind(PeopleValue? people) {
    _nameController.value = people?.name;
    _heightController.value = people?.height;
    _massController.value = people?.mass;
    _hairColorController.value = people?.hairColor;
    _skinColorController.value = people?.skinColor;
    _eyeColorController.value = people?.eyeColor;
    _birthYearController.value = people?.birthYear;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: widget.formKey,
        onChanged: () {
          widget.controller._name = _nameController.value;
          widget.controller._height = _heightController.value;
          widget.controller._mass = _massController.value;
          widget.controller._hairColor = _hairColorController.value;
          widget.controller._skinColor = _skinColorController.value;
          widget.controller._eyeColor = _eyeColorController.value;
          widget.controller._birthYear = _birthYearController.value;
          widget.controller._update();
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomTextFormField(
              context: context,
              label: 'Nama',
              hint: 'suparno',
              controller: _nameController,
            ),
            CustomTextFormField(
              context: context,
              label: 'Tinggi',
              isDigit: true,
              hint: '168 cm',
              controller: _heightController,
            ),
            CustomTextFormField(
              context: context,
              isDigit: true,
              label: 'Berat Badan',
              hint: '50 Kg',
              controller: _massController,
            ),
            CustomTextFormField(
              context: context,
              label: 'Warna Rambut',
              hint: 'warna rambut',
              controller: _hairColorController,
            ),
            CustomTextFormField(
              context: context,
              label: 'Warna Kulit',
              hint: 'warna kulit',
              controller: _skinColorController,
            ),
            CustomTextFormField(
              context: context,
              label: 'Warna Mata',
              hint: 'warna mata',
              controller: _eyeColorController,
            ),
            CustomTextFormField(
              context: context,
              label: 'Tahun Lahir',
              hint: 'tahun lahir',
              controller: _birthYearController,
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _nameController.dispose();
    _heightController.dispose();
    _massController.dispose();
    _hairColorController.dispose();
    _skinColorController.dispose();
    _eyeColorController.dispose();
    _birthYearController.dispose();
    super.dispose();
  }
}

class PeopleValue {
  final String name;
  final String height;
  final String mass;
  final String hairColor;
  final String skinColor;
  final String eyeColor;
  final String birthYear;
  final List<String> films;
  final List<String> species;

  PeopleValue({
    required this.name,
    required this.height,
    required this.mass,
    required this.hairColor,
    required this.skinColor,
    required this.eyeColor,
    required this.birthYear,
    required this.species,
    required this.films,
  });
}

class PeopleController extends ValueNotifier<PeopleValue?> {
  String? _name;
  String? _height;
  String? _mass;
  String? _hairColor;
  String? _skinColor;
  String? _eyeColor;
  String? _birthYear;
  List<String>? _films;
  List<String>? _species;

  PeopleController({PeopleValue? initialValue}) : super(initialValue);

  set data(People people) {
    _name = people.name;
    _height = people.height;
    _mass = people.mass;
    _hairColor = people.hairColor;
    _skinColor = people.skinColor;
    _eyeColor = people.eyeColor;
    _birthYear = people.birthYear;
    _films = people.films;
    _species = people.species;
    _update();
  }

  void _update() {
    final _name = this._name;
    final _height = this._height;
    final _mass = this._mass;
    final _hairColor = this._hairColor;
    final _skinColor = this._skinColor;
    final _eyeColor = this._eyeColor;
    final _birthYear = this._birthYear;

    if (_name != null &&
        _height != null &&
        _mass != null &&
        _hairColor != null &&
        _skinColor != null &&
        _eyeColor != null &&
        _birthYear != null) {
      value = PeopleValue(
        name: _name,
        height: _height,
        mass: _mass,
        hairColor: _hairColor,
        skinColor: _skinColor,
        eyeColor: _eyeColor,
        birthYear: _birthYear,
        films: _films ?? [],
        species: _species ?? [],
      );
    } else {
      value = null;
    }
  }
}
