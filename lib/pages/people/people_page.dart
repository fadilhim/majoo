import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo/bloc/auth/auth_cubit.dart';
import 'package:majoo/bloc/people/people_cubit.dart';
import 'package:majoo/model/people.dart';
import 'package:majoo/pages/people/add_people_page.dart';
import 'package:majoo/pages/people/detail_people_page.dart';
import 'package:majoo/utilities/ui_utils.dart';
import 'package:majoo/widgets/custom_appbar.dart';
import 'package:majoo/widgets/dialog/error_dialog.dart';
import 'package:majoo/widgets/empty_widget.dart';
import 'package:majoo/widgets/loading_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class PeoplePage extends StatefulWidget {
  @override
  _PeoplePageState createState() => _PeoplePageState();
}

class _PeoplePageState extends State<PeoplePage> {
  TextEditingController valueSearchController = new TextEditingController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    BlocProvider.of<PeopleCubit>(context).initGet();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        customTitle: searchContainer(),
        actions: [
          IconButton(
            icon: BlocBuilder<PeopleCubit, PeopleState>(
              builder: (context, state) {
                final viewMode = context.watch<PeopleCubit>().viewMode;
                return Icon(
                  viewMode == ViewMode.grid ? Icons.grid_view : Icons.list,
                  color: Color(0xff323232),
                );
              },
            ),
            onPressed: () => BlocProvider.of<PeopleCubit>(context).changeView(),
          ),
          IconButton(
            icon: const Icon(
              Icons.add,
              color: Color(0xff323232),
            ),
            onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CreatePeoplePage(),
              ),
            ),
          )
        ],
      ),
      resizeToAvoidBottomInset: false,
      body: BlocConsumer<PeopleCubit, PeopleState>(
        listener: (context, state) {
          if (state is PeopleLoadFailure) {
            ErrorDialog.showDialog(context, message: state.error.getMessage());
          }
          if (state is MaxPeopleLoadSuccess) {
            ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text('Data sudah mencapai max')));
          }
        },
        builder: (context, state) {
          return StreamBuilder(
            stream: BlocProvider.of<PeopleCubit>(context).peoples,
            builder:
                (BuildContext context, AsyncSnapshot<List<People>> snapshot) {
              final _data = snapshot.data;
              List<People> peoples = [];
              final view = context.watch<PeopleCubit>().viewMode;

              if (_data != null) {
                peoples = _data;
              }
              if (state is PeopleLoadInProgress) {
                return Center(child: LoadingWidget());
              }
              if (state is SearchPeopleLoadSuccess) {
                peoples = state.peopleList;
              }
              if (peoples.isEmpty) {
                return Center(child: EmptyWidget());
              }
              _refreshController.loadComplete();
              return Column(children: [
                Flexible(
                  child: SmartRefresher(
                    enablePullUp: true,
                    enablePullDown: false,
                    controller: _refreshController,
                    onLoading: _onLoading,
                    child: view == ViewMode.list
                        ? ListView.builder(
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                            itemBuilder: (cxt, idx) {
                              return listViewCard(
                                  people: peoples[idx], index: idx);
                            },
                            itemCount: peoples.length,
                          )
                        : GridView.builder(
                            gridDelegate:
                                SliverGridDelegateWithMaxCrossAxisExtent(
                              maxCrossAxisExtent: 200,
                              childAspectRatio: 1 / 1.3,
                              crossAxisSpacing: 20,
                            ),
                            itemCount: peoples.length,
                            itemBuilder: (context, index) {
                              return gridViewCard(
                                  index: index, people: peoples[index]);
                            },
                          ),
                  ),
                ),
              ]);
            },
          );
        },
      ),
    );
  }

  Widget searchContainer() {
    return Container(
      constraints: BoxConstraints(
        maxWidth: ResponsiveUi.dialogMaxWidth,
      ),
      margin: EdgeInsets.symmetric(vertical: 8),
      decoration: BoxDecoration(
        color: Colors.blue.withOpacity(0.1),
        borderRadius: BorderRadius.circular(12),
      ),
      child: Form(
        key: formKey,
        child: TextFormField(
          controller: valueSearchController,
          keyboardType: TextInputType.text,
          onFieldSubmitted: (value) => onSearch(),
          decoration: InputDecoration(
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            hintText: 'Search name',
            suffixIcon:
                IconButton(onPressed: onSearch, icon: Icon(Icons.search)),
            contentPadding: EdgeInsets.all(20),
          ),
        ),
      ),
    );
  }

  Widget listViewCard({required People people, required int index}) {
    Widget detailContainer({required String left, required String right}) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            left,
            style: TextStyle(
              fontSize: 11,
            ),
          ),
          Text(
            right,
            style: TextStyle(
              fontSize: 11,
            ),
          )
        ],
      );
    }

    final favorites = context.watch<PeopleCubit>().favorites;
    final isFavorite = favorites.contains(people.id);

    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DetailPeoplePage(people)),
        );
      },
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 9,
        ),
        width: double.infinity,
        height: 125,
        color: index.isEven ? Color(0xffF8F8F8) : Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image.network(
              "https://i.pinimg.com/736x/65/25/a0/6525a08f1df98a2e3a545fe2ace4be47.jpg",
              height: 100,
              width: 100,
            ),
            SizedBox(width: 9),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          people.name,
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        GestureDetector(
                          onTap: () => BlocProvider.of<PeopleCubit>(context)
                              .edit(people: people, isFavorite: !isFavorite),
                          child: Icon(
                            isFavorite
                                ? Icons.favorite
                                : Icons.favorite_outline,
                            color: isFavorite ? Colors.pink : null,
                          ),
                        )
                      ]),
                  detailContainer(left: 'Berat', right: '${people.mass} KG'),
                  detailContainer(left: 'Tinggi', right: '${people.height} cm'),
                  detailContainer(left: 'Warna Mata', right: people.eyeColor),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget gridViewCard({required People people, required int index}) {
    final favorites = context.watch<PeopleCubit>().favorites;
    final isFavorite = favorites.contains(people.id);
    return Stack(children: [
      InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => DetailPeoplePage(people)),
          );
        },
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.center,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(6),
                  child: Image.network(
                    "https://i.pinimg.com/736x/65/25/a0/6525a08f1df98a2e3a545fe2ace4be47.jpg",
                    height: 150,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SizedBox(height: 6),
              Text(
                people.name,
                style:
                    TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 6),
              Text(people.gender,
                  style: TextStyle(fontWeight: FontWeight.bold)),
              Text(
                people.birthYear,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.grey,
                    fontSize: 12),
              ),
            ],
          ),
        ),
      ),
      Positioned(
        top: 0,
        right: 5,
        child: GestureDetector(
          onTap: () => BlocProvider.of<PeopleCubit>(context)
              .edit(people: people, isFavorite: !isFavorite),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.3),
              shape: BoxShape.circle,
            ),
            padding: EdgeInsets.all(3),
            child: Icon(
              isFavorite ? Icons.favorite : Icons.favorite_outline,
              color: isFavorite ? Colors.pink : null,
            ),
          ),
        ),
      ),
    ]);
  }

  void _onLoading() async {
    await BlocProvider.of<PeopleCubit>(context).loadMore();
  }

  void onSearch() async {
    await BlocProvider.of<PeopleCubit>(context)
        .search(name: valueSearchController.text);
  }
}
