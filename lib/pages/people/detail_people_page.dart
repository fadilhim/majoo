import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo/bloc/people/people_cubit.dart';
import 'package:majoo/model/people.dart';
import 'package:majoo/pages/film/film_page.dart';
import 'package:majoo/pages/people/edit_people_page.dart';
import 'package:majoo/pages/species/species_page.dart';
import 'package:majoo/widgets/custom_appbar.dart';
import 'package:majoo/widgets/dialog/confirmation_dialog.dart';
import 'package:majoo/widgets/dialog/success_dialog.dart';
import 'package:majoo/utilities/id_utils.dart';

class DetailPeoplePage extends StatefulWidget {
  final People people;

  DetailPeoplePage(this.people);

  @override
  _DetailPeoplePageState createState() => _DetailPeoplePageState();
}

class _DetailPeoplePageState extends State<DetailPeoplePage> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<PeopleCubit, PeopleState>(
      listener: (context, state) {
        if (state is DeletePeopleLoadSuccess) {
          Navigator.pop(context);
          SuccessDialog.showDialog(
            context,
            message: 'Berhasil menghapus ${widget.people.name}',
          );
        }
      },
      child: Scaffold(
        backgroundColor: Color(0xffF8F8F8),
        appBar: CustomAppBar(
          title: 'Detail People',
          actions: _actionButton,
        ),
        resizeToAvoidBottomInset: false,
        body: SafeArea(
            child: Padding(
          padding: EdgeInsets.symmetric(vertical: 9, horizontal: 12),
          child: SingleChildScrollView(child: _body()),
        )),
      ),
    );
  }

  List<Widget> get _actionButton {
    final children = <Widget>[
      IconButton(
        icon: const Icon(
          Icons.edit_outlined,
          color: Colors.blue,
        ),
        onPressed: () => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => EditPeoplePage(
              people: widget.people,
            ),
          ),
        ),
      ),
      IconButton(
        icon: const Icon(
          Icons.delete_outline,
          color: Colors.red,
        ),
        onPressed: () {
          SimpleConfirmationDialog.showDialog(
            context,
            message: 'Anda yakin ingin menghapus ${widget.people.name} ?',
            onConfirm: () {
              BlocProvider.of<PeopleCubit>(context).delete(widget.people);
            },
          );
        },
      ),
    ];

    return children;
  }

  Widget _body() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Align(
          alignment: Alignment.center,
          child: Image.network(
            "https://i.pinimg.com/736x/65/25/a0/6525a08f1df98a2e3a545fe2ace4be47.jpg",
            height: 100,
          ),
        ),
        SizedBox(height: 9),
        Divider(
          color: Colors.black12,
        ),
        SizedBox(height: 9),
        ..._detailContainer,
      ],
    );
  }

  List<Widget> get _detailContainer {
    final people = widget.people;
    return [
      featureContainer(
        'Nama',
        people.name,
        Icons.face,
      ),
      featureContainer(
        'Jenis Kelamin',
        people.gender,
        Icons.email_outlined,
      ),
      featureContainer(
        'Berat Badan',
        '${people.mass} Kg',
        Icons.line_weight,
      ),
      featureContainer(
        'Tinggi',
        '${people.height} cm',
        Icons.timer_sharp,
      ),
      featureContainer(
        'Warna Kulit',
        people.skinColor,
        Icons.water,
      ),
      featureContainer(
        'Warna Mata',
        people.eyeColor,
        Icons.water,
      ),
      featureContainer(
        'Warna Rambut',
        people.hairColor,
        Icons.water,
      ),
      featureContainer(
        'Tahun Lahir',
        people.birthYear,
        Icons.date_range,
      ),
      if (widget.people.films.length > 0) SizedBox(height: 8),
      if (widget.people.films.length > 0)
        Text(
          'List Movie',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
        ),
      filmContainer(),
      if (widget.people.species.length > 0) SizedBox(height: 8),
      if (widget.people.species.length > 0)
        Text(
          'List Species',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
        ),
      speciesContainer(),
    ];
  }

  Widget featureContainer(String title, String description, IconData icon) {
    return InkWell(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: 20),
                      child: Icon(icon, size: 25),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: Text(
                        title,
                        style: TextStyle(
                          fontSize: 14.5,
                          color: Colors.black54,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: Text(description),
                )
              ],
            ),
          ),
          Divider(
            color: Colors.black12,
          )
        ],
      ),
    );
  }

  Widget filmContainer() {
    final films = widget.people.films;
    final children = <Widget>[];
    for (final film in films) {
      children.addAll([
        Row(
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.blue,
                shape: BoxShape.circle,
              ),
              margin: EdgeInsets.only(right: 8),
              height: 10,
              width: 10,
            ),
            InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DetailFilmPage(film)));
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 4),
                child: Text(
                  'film ${film.idFormat}',
                  style: TextStyle(
                    fontSize: 14.5,
                    color: Colors.black54,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 4),
      ]);
    }
    return Column(children: children);
  }

  Widget speciesContainer() {
    final species = widget.people.species;
    return ListView.separated(
      shrinkWrap: true,
      itemCount: species.length,
      separatorBuilder: (context, index) {
        return Divider(height: 2, thickness: 2);
      },
      itemBuilder: (context, index) {
        final _species = species[index];
        return Row(
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.blue,
                shape: BoxShape.circle,
              ),
              margin: EdgeInsets.only(right: 8),
              height: 10,
              width: 10,
            ),
            InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DetailSpeciesPage(_species)));
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 4),
                child: Text(
                  'species ${_species.idFormat}',
                  style: TextStyle(
                    fontSize: 14.5,
                    color: Colors.black54,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            )
          ],
        );
      },
    );
  }
}
