import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo/bloc/people/people_cubit.dart';
import 'package:majoo/pages/people/people_form.dart';
import 'package:majoo/widgets/custom_appbar.dart';
import 'package:majoo/widgets/custom_button.dart';
import 'package:majoo/widgets/dialog/success_dialog.dart';

class CreatePeoplePage extends StatefulWidget {
  CreatePeoplePage({Key? key});

  @override
  _CreatePeoplePageState createState() => _CreatePeoplePageState();
}

class _CreatePeoplePageState extends State<CreatePeoplePage> {
  final PeopleController controller = PeopleController();

  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: 'Add People'),
      body: BlocConsumer<PeopleCubit, PeopleState>(
        listener: (context, state) {
          if (state is CreatePeopleLoadSuccess) {
            Navigator.pop(context);
            SuccessDialog.showDialog(context, message: 'Success Create People');
          }
        },
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.symmetric(vertical: 9, horizontal: 12),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 12,
                  child: PeopleForm(
                    controller: controller,
                    formKey: formKey,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: CustomButton(
                    onPressed: _onSubmit,
                    text: 'Submit',
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  void _onSubmit() {
    final data = controller.value;
    if (formKey.currentState?.validate() == true && data != null) {
      BlocProvider.of<PeopleCubit>(context).add(
        name: data.name,
        height: data.height,
        mass: data.mass,
        hairColor: data.hairColor,
        skinColor: data.skinColor,
        eyeColor: data.eyeColor,
        birthYear: data.birthYear,
      );
    }
  }
}
