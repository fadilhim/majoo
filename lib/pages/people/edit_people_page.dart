import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo/bloc/people/people_cubit.dart';
import 'package:majoo/model/people.dart';
import 'package:majoo/pages/people/people_form.dart';
import 'package:majoo/widgets/custom_appbar.dart';
import 'package:majoo/widgets/custom_button.dart';
import 'package:majoo/widgets/dialog/success_dialog.dart';

class EditPeoplePage extends StatefulWidget {
  final People people;

  EditPeoplePage({Key? key, required this.people});

  @override
  _EditPeoplePageState createState() => _EditPeoplePageState();
}

class _EditPeoplePageState extends State<EditPeoplePage> {
  final PeopleController controller = PeopleController();

  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    controller.data = widget.people;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: 'Edit People'),
      body: BlocListener<PeopleCubit, PeopleState>(
        listener: (context, state) {
          if (state is EditPeopleLoadSuccess) {
            Navigator.pop(context);
            Navigator.pop(context);
            SuccessDialog.showDialog(context,
                message: 'Success Edit People');
          }
        },
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 9, horizontal: 12),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 12,
                child: PeopleForm(
                  controller: controller,
                  formKey: formKey,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: CustomButton(
                  onPressed: _onSubmit,
                  text: 'Edit People',
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onSubmit() {
    final data = controller.value;
    if (formKey.currentState?.validate() == true && data != null) {
      BlocProvider.of<PeopleCubit>(context).edit(
        people: widget.people,
        birthYear: data.birthYear,
        eyeColor: data.eyeColor,
        skinColor: data.skinColor,
        hairColor: data.skinColor,
        mass: data.mass,
        height: data.height,
        name: data.name,
      );
    }
  }
}
