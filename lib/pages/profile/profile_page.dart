import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo/bloc/auth/auth_cubit.dart';
import 'package:majoo/model/user.dart';
import 'package:majoo/pages/login/login_page.dart';
import 'package:majoo/widgets/custom_button.dart';
import 'package:provider/provider.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<User?>(
      future: BlocProvider.of<AuthCubit>(context).getLoggedUser(),
      builder: (BuildContext context, AsyncSnapshot<User?> snapshot) {
        final user = snapshot.data;
        return Scaffold(
          resizeToAvoidBottomInset: false,
          body: SafeArea(
            child: SingleChildScrollView(
              padding:
                  EdgeInsets.only(top: 15, bottom: 15, right: 10, left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Akun', style: Theme.of(context).textTheme.headline5),
                  SizedBox(height: 12),
                  Align(
                    alignment: Alignment.center,
                    child: avatarContainer(),
                  ),
                  SizedBox(height: 9),
                  Divider(
                    color: Colors.black12,
                  ),
                  SizedBox(height: 50),
                  featureContainer(
                    'Nama',
                    user?.name ?? '',
                    Icons.face,
                  ),
                  featureContainer(
                    'Email',
                    user?.email ?? '',
                    Icons.email_outlined,
                  ),
                  featureContainer(
                    'Berat Badan',
                    user?.mass != null ? '${user?.mass.toString()} Kg' : '',
                    Icons.line_weight,
                  ),
                  featureContainer(
                    'Umur',
                    user?.age.toString() ?? '',
                    Icons.timer_sharp,
                  ),
                  SizedBox(height: 50),
                  CustomButton.outline(
                    text: 'Log Out',
                    onPressed: doLogOut,
                    isSecondary: true,
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget avatarContainer() {
    return Container(
      width: 75,
      height: 75,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          image: NetworkImage(
              "https://i.pinimg.com/736x/65/25/a0/6525a08f1df98a2e3a545fe2ace4be47.jpg"),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget featureContainer(String title, String description, IconData icon) {
    return InkWell(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: 20),
                      child: Icon(icon, size: 25),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: Text(
                        title,
                        style: TextStyle(
                          fontSize: 14.5,
                          color: Colors.black54,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: Text(description),
                )
              ],
            ),
          ),
          Divider(
            color: Colors.black12,
          )
        ],
      ),
    );
  }

  void doLogOut() async {
    await context.read<AuthCubit>().logOut();

    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (context) => LoginPage()), (route) => false);
  }
}
