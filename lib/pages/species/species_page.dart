import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo/bloc/species/species_cubit.dart';
import 'package:majoo/model/species.dart';
import 'package:majoo/widgets/custom_appbar.dart';
import 'package:majoo/utilities/datetime_utils.dart';
import 'package:majoo/widgets/dialog/error_dialog.dart';
import 'package:majoo/widgets/loading_widget.dart';

class DetailSpeciesPage extends StatefulWidget {
  final String speciesId;

  DetailSpeciesPage(this.speciesId);

  @override
  _DetailSpeciesPageState createState() => _DetailSpeciesPageState();
}

class _DetailSpeciesPageState extends State<DetailSpeciesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF8F8F8),
      appBar: CustomAppBar(title: 'Detail Species'),
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 9, horizontal: 12),
          child: _body(),
        ),
      ),
    );
  }

  Widget _body() {
    return BlocProvider<SpeciesCubit>(
      create: (context) => SpeciesCubit.create(context, widget.speciesId),
      child: BlocConsumer<SpeciesCubit, SpeciesState>(
        listener: (context, state) {
          if (state is SpeciesLoadFailure) {
            ErrorDialog.showDialog(
              context,
              message: state.error.getMessage(),
            );
          }
        },
        builder: (context, state) {
          if (state is SpeciesLoadInProgress) {
            return Expanded(child: Center(child: LoadingWidget()));
          } else if (state is SpeciesLoadSuccess) {
            return SafeArea(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 9, horizontal: 12),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Image.network(
                          "https://fiverr-res.cloudinary.com/images/t_main1,q_auto,f_auto,q_auto,f_auto/gigs/206523336/original/f4649ba811b9d926ea712381d2bf5cf95a7feaa7/do-a-landscape-minimalist-movie-poster-and-artwork.jpg",
                          height: 100,
                        ),
                      ),
                      SizedBox(height: 9),
                      Divider(
                        color: Colors.black12,
                      ),
                      SizedBox(height: 9),
                      ..._detailContainer(state.species)
                    ],
                  ),
                ),
              ),
            );
          }
          return Expanded(
              child: Center(
                  child: ErrorWidget(
                      'Sepertinya sedang ada gangguan di server kami, mohon kembali nanti')));
        },
      ),
    );
  }

  List<Widget> _detailContainer(Species species) {
    return [
      featureContainer(
        'Nama',
        species.name,
        Icons.face,
      ),
      featureContainer(
        'Warna Rambut',
        species.hairColor,
        Icons.invert_colors,
      ),
      featureContainer(
        'Warna Mata',
        species.eyeColor,
        Icons.invert_colors,
      ),
      featureContainer(
        'Warna Kulit',
        species.skinColor,
        Icons.invert_colors,
      ),
      featureContainer(
        'Bahasa',
        species.language,
        Icons.language,
      ),
      featureContainer(
        'Klasifikasi',
        species.classification,
        Icons.crop_landscape_sharp,
      ),
      featureContainer(
        'Designation',
        species.designation,
        Icons.drive_file_rename_outline,
      ),
      featureContainer(
        'Rata-rata Tinggi',
        species.averageHeight,
        Icons.height,
      ),
      featureContainer(
        'Rata-rata Jangka Hidup',
        species.averageLifespan,
        Icons.accessibility,
      ),
    ];
  }

  Widget featureContainer(String title, String description, IconData icon) {
    return InkWell(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: 20),
                      child: Icon(icon, size: 25),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: Text(
                        title,
                        style: TextStyle(
                          fontSize: 14.5,
                          color: Colors.black54,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: Text(description),
                )
              ],
            ),
          ),
          Divider(
            color: Colors.black12,
          )
        ],
      ),
    );
  }
}
