import 'package:flutter/material.dart';
import 'package:majoo/pages/favorite/favorite_page.dart';
import 'package:majoo/pages/people/people_page.dart';
import 'package:majoo/pages/profile/profile_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final menus = [
    PeoplePage(),
    FavoritePage(),
    ProfilePage(),
  ];
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: menus[_currentIndex],
      bottomNavigationBar: bottomNavigation,
    );
  }

  Widget get bottomNavigation {
    return BottomAppBar(
      shape: CircularNotchedRectangle(),
      color: Colors.white,
      child: IconTheme(
        data: IconThemeData(color: Colors.blue),
        child: Builder(
          builder: (context) {
            final children = <Widget>[
              navigationContainer(
                index: 0,
                title: 'Home',
                firstIcon: Icons.home_filled,
                secondIcon: Icons.home_outlined,
              ),
              SizedBox(
                width: 50,
                height: 50,
              ),
              navigationContainer(
                index: 1,
                title: 'Favorit',
                firstIcon: Icons.favorite,
                secondIcon: Icons.favorite_outline,
              ),
              SizedBox(
                width: 50,
                height: 50,
              ),
              navigationContainer(
                index: 2,
                title: 'Profile',
                firstIcon: Icons.person,
                secondIcon: Icons.person_outline,
              ),
            ];

            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.max,
              children: children,
            );
          },
        ),
      ),
    );
  }

  Widget navigationContainer({
    required int index,
    required String title,
    required IconData firstIcon,
    required IconData secondIcon,
  }) {
    return InkWell(
      onTap: () {
        setState(() {
          _currentIndex = index;
        });
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(
            _currentIndex == index ? firstIcon : secondIcon,
            color: _currentIndex == index ? Colors.blue : Colors.grey,
          ),
          Text(
            title,
            style: TextStyle(
              color: _currentIndex == index ? Colors.blue : Colors.grey,
              fontSize: 12,
            ),
          ),
        ],
      ),
    );
  }
}
