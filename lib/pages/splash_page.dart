import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:majoo/pages/home/home_page.dart';
import 'package:majoo/pages/login/login_page.dart';

class SplashPage extends StatefulWidget {
  final bool isLoggedIn;

  SplashPage({required this.isLoggedIn});

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with TickerProviderStateMixin {
  var expanded = false;
  double _bigFontSize = kIsWeb ? 234 : 178;
  final transitionDuration = Duration(seconds: 1);

  _navigate(Widget page) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => page), (route) => false);
  }

  @override
  void initState() {
    Future.delayed(Duration(seconds: 3))
        .then((value) => setState(() => expanded = true))
        .then(
          (value) => Future.delayed(Duration(seconds: 2)).then((value) =>
              widget.isLoggedIn
                  ? _navigate(HomePage())
                  : _navigate(LoginPage())),
        );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AnimatedDefaultTextStyle(
              duration: transitionDuration,
              curve: Curves.fastOutSlowIn,
              style: TextStyle(
                color: Colors.black,
                fontSize: !expanded ? _bigFontSize : 50,
                fontWeight: FontWeight.w600,
              ),
              child: Text(
                'M',
              ),
            ),
            AnimatedCrossFade(
              firstCurve: Curves.fastOutSlowIn,
              crossFadeState: !expanded
                  ? CrossFadeState.showFirst
                  : CrossFadeState.showSecond,
              duration: transitionDuration,
              firstChild: Container(),
              secondChild: _logoRemainder(),
              alignment: Alignment.centerLeft,
              sizeCurve: Curves.easeInOut,
            ),
          ],
        ),
      ),
    );
  }

  Widget _logoRemainder() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          'AJOO',
          style: TextStyle(
            color: Colors.black,
            fontSize: 50,
            // fontFamily: 'Montserrat',
            fontWeight: FontWeight.w600,
          ),
        ),
      ],
    );
  }
}
