import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo/bloc/favorite/favorite_cubit.dart';
import 'package:majoo/model/people.dart';
import 'package:majoo/widgets/custom_appbar.dart';
import 'package:majoo/widgets/empty_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class FavoritePage extends StatefulWidget {
  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  TextEditingController valueSearchController = new TextEditingController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    BlocProvider.of<FavoriteCubit>(context).initGet();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: 'Favorit'),
      resizeToAvoidBottomInset: false,
      body: StreamBuilder(
        stream: context.read<FavoriteCubit>().favorites,
        builder: (BuildContext context, AsyncSnapshot<List<People>> snapshot) {
          final _data = snapshot.data;
          List<People> peoples = [];
          if (_data != null) {
            peoples = _data;
          }
          if (peoples.isEmpty) {
            return Center(child: EmptyWidget());
          }
          _refreshController.loadComplete();
          return Column(children: [
            Flexible(
              child: SmartRefresher(
                enablePullUp: false,
                enablePullDown: false,
                controller: _refreshController,
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemBuilder: (cxt, idx) {
                    return card(people: peoples[idx], index: idx);
                  },
                  itemCount: peoples.length,
                ),
              ),
            ),
          ]);
        },
      ),
    );
  }

  Widget card({required People people, required int index}) {
    Widget detailContainer({required String left, required String right}) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            left,
            style: TextStyle(
              fontSize: 11,
            ),
          ),
          Text(
            right,
            style: TextStyle(
              fontSize: 11,
            ),
          )
        ],
      );
    }

    return InkWell(
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 9,
        ),
        width: double.infinity,
        height: 125,
        color: index.isEven ? Color(0xffF8F8F8) : Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image.network(
              "https://i.pinimg.com/736x/65/25/a0/6525a08f1df98a2e3a545fe2ace4be47.jpg",
              height: 100,
              width: 100,
            ),
            SizedBox(width: 9),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          people.name,
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        GestureDetector(
                          onTap: () => BlocProvider.of<FavoriteCubit>(context)
                              .delete(people),
                          child: Icon(
                            Icons.favorite,
                            color: Colors.pink,
                          ),
                        )
                      ]),
                  detailContainer(left: 'Berat', right: '${people.mass} KG'),
                  detailContainer(left: 'Tinggi', right: '${people.height} cm'),
                  detailContainer(left: 'Warna Mata', right: people.eyeColor),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
