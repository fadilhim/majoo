import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo/bloc/auth/auth_cubit.dart';
import 'package:majoo/pages/home/home_page.dart';
import 'package:majoo/widgets/custom_button.dart';
import 'package:majoo/widgets/dialog/error_dialog.dart';
import 'package:majoo/widgets/dialog/loading_dialog.dart';
import 'package:majoo/widgets/dialog/success_dialog.dart';
import 'package:majoo/widgets/form_field/text_form_field.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<RegisterPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  final _nameController = TextController();
  final _heightController = TextController();
  final _massController = TextController();
  final _ageController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthCubit, AuthState>(
        listener: (context, state) {
          if (state is RegisterLoadInProgress) {
            LoadingDialog.showDialog(context);
          } else if (state is RegisterLoadSuccess) {
            Navigator.pop(context);
            Navigator.pop(context, {
              'email': _emailController.value,
              'password': _passwordController.value
            });
            // SuccessDialog.showDialog(context, message: 'berhasil');
          } else if (state is RegisterLoadFailure) {
            Navigator.pop(context);
            ErrorDialog.showDialog(context, message: state.error.getMessage());
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan daftarkan akun anda',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                CustomButton(
                  text: 'Register',
                  onPressed: handleRegister,
                  height: 100,
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _nameController,
            hint: 'Eka Saputra',
            label: 'Nama',
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'email is invalid';
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
          CustomTextFormField(
            context: context,
            isDigit: true,
            controller: _heightController,
            hint: '170',
            label: 'Tinggi',
          ),
          CustomTextFormField(
            context: context,
            isDigit: true,
            controller: _massController,
            hint: '70',
            label: 'Berat Badan',
          ),
          CustomTextFormField(
            context: context,
            isDigit: true,
            controller: _ageController,
            hint: '21',
            label: 'Umur',
          ),
        ],
      ),
    );
  }

  void handleRegister() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;
    final _name = _nameController.value;
    final _height = _heightController.value;
    final _age = _ageController.value;
    final _mass = _massController.value;

    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null &&
        _name != null &&
        _height != null &&
        _age != null &&
        _mass != null) {
      await context.read<AuthCubit>().register(
            name: _name,
            password: _password,
            email: _email,
            age: int.parse(_age),
            height: int.parse(_height),
            mass: int.parse(_mass),
          );
    }
  }
}
