// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'film.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Film _$_$_FilmFromJson(Map<String, dynamic> json) {
  return _$_Film(
    title: json['title'] as String,
    director: json['director'] as String,
    producer: json['producer'] as String,
    openingCrawl: json['opening_crawl'] as String,
    episodeId: json['episode_id'] as int,
    releaseDate: DateTime.parse(json['release_date'] as String),
    characters:
        (json['characters'] as List<dynamic>).map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$_$_FilmToJson(_$_Film instance) => <String, dynamic>{
      'title': instance.title,
      'director': instance.director,
      'producer': instance.producer,
      'opening_crawl': instance.openingCrawl,
      'episode_id': instance.episodeId,
      'release_date': instance.releaseDate.toIso8601String(),
      'characters': instance.characters,
    };
