// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'film.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Film _$FilmFromJson(Map<String, dynamic> json) {
  return _Film.fromJson(json);
}

/// @nodoc
class _$FilmTearOff {
  const _$FilmTearOff();

  _Film call(
      {required String title,
      required String director,
      required String producer,
      @JsonKey(name: 'opening_crawl') required String openingCrawl,
      @JsonKey(name: 'episode_id') required int episodeId,
      @JsonKey(name: 'release_date') required DateTime releaseDate,
      required List<String> characters}) {
    return _Film(
      title: title,
      director: director,
      producer: producer,
      openingCrawl: openingCrawl,
      episodeId: episodeId,
      releaseDate: releaseDate,
      characters: characters,
    );
  }

  Film fromJson(Map<String, Object> json) {
    return Film.fromJson(json);
  }
}

/// @nodoc
const $Film = _$FilmTearOff();

/// @nodoc
mixin _$Film {
  String get title => throw _privateConstructorUsedError;
  String get director => throw _privateConstructorUsedError;
  String get producer => throw _privateConstructorUsedError;
  @JsonKey(name: 'opening_crawl')
  String get openingCrawl => throw _privateConstructorUsedError;
  @JsonKey(name: 'episode_id')
  int get episodeId => throw _privateConstructorUsedError;
  @JsonKey(name: 'release_date')
  DateTime get releaseDate => throw _privateConstructorUsedError;
  List<String> get characters => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FilmCopyWith<Film> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FilmCopyWith<$Res> {
  factory $FilmCopyWith(Film value, $Res Function(Film) then) =
      _$FilmCopyWithImpl<$Res>;
  $Res call(
      {String title,
      String director,
      String producer,
      @JsonKey(name: 'opening_crawl') String openingCrawl,
      @JsonKey(name: 'episode_id') int episodeId,
      @JsonKey(name: 'release_date') DateTime releaseDate,
      List<String> characters});
}

/// @nodoc
class _$FilmCopyWithImpl<$Res> implements $FilmCopyWith<$Res> {
  _$FilmCopyWithImpl(this._value, this._then);

  final Film _value;
  // ignore: unused_field
  final $Res Function(Film) _then;

  @override
  $Res call({
    Object? title = freezed,
    Object? director = freezed,
    Object? producer = freezed,
    Object? openingCrawl = freezed,
    Object? episodeId = freezed,
    Object? releaseDate = freezed,
    Object? characters = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      director: director == freezed
          ? _value.director
          : director // ignore: cast_nullable_to_non_nullable
              as String,
      producer: producer == freezed
          ? _value.producer
          : producer // ignore: cast_nullable_to_non_nullable
              as String,
      openingCrawl: openingCrawl == freezed
          ? _value.openingCrawl
          : openingCrawl // ignore: cast_nullable_to_non_nullable
              as String,
      episodeId: episodeId == freezed
          ? _value.episodeId
          : episodeId // ignore: cast_nullable_to_non_nullable
              as int,
      releaseDate: releaseDate == freezed
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      characters: characters == freezed
          ? _value.characters
          : characters // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
abstract class _$FilmCopyWith<$Res> implements $FilmCopyWith<$Res> {
  factory _$FilmCopyWith(_Film value, $Res Function(_Film) then) =
      __$FilmCopyWithImpl<$Res>;
  @override
  $Res call(
      {String title,
      String director,
      String producer,
      @JsonKey(name: 'opening_crawl') String openingCrawl,
      @JsonKey(name: 'episode_id') int episodeId,
      @JsonKey(name: 'release_date') DateTime releaseDate,
      List<String> characters});
}

/// @nodoc
class __$FilmCopyWithImpl<$Res> extends _$FilmCopyWithImpl<$Res>
    implements _$FilmCopyWith<$Res> {
  __$FilmCopyWithImpl(_Film _value, $Res Function(_Film) _then)
      : super(_value, (v) => _then(v as _Film));

  @override
  _Film get _value => super._value as _Film;

  @override
  $Res call({
    Object? title = freezed,
    Object? director = freezed,
    Object? producer = freezed,
    Object? openingCrawl = freezed,
    Object? episodeId = freezed,
    Object? releaseDate = freezed,
    Object? characters = freezed,
  }) {
    return _then(_Film(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      director: director == freezed
          ? _value.director
          : director // ignore: cast_nullable_to_non_nullable
              as String,
      producer: producer == freezed
          ? _value.producer
          : producer // ignore: cast_nullable_to_non_nullable
              as String,
      openingCrawl: openingCrawl == freezed
          ? _value.openingCrawl
          : openingCrawl // ignore: cast_nullable_to_non_nullable
              as String,
      episodeId: episodeId == freezed
          ? _value.episodeId
          : episodeId // ignore: cast_nullable_to_non_nullable
              as int,
      releaseDate: releaseDate == freezed
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      characters: characters == freezed
          ? _value.characters
          : characters // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Film extends _Film {
  const _$_Film(
      {required this.title,
      required this.director,
      required this.producer,
      @JsonKey(name: 'opening_crawl') required this.openingCrawl,
      @JsonKey(name: 'episode_id') required this.episodeId,
      @JsonKey(name: 'release_date') required this.releaseDate,
      required this.characters})
      : super._();

  factory _$_Film.fromJson(Map<String, dynamic> json) =>
      _$_$_FilmFromJson(json);

  @override
  final String title;
  @override
  final String director;
  @override
  final String producer;
  @override
  @JsonKey(name: 'opening_crawl')
  final String openingCrawl;
  @override
  @JsonKey(name: 'episode_id')
  final int episodeId;
  @override
  @JsonKey(name: 'release_date')
  final DateTime releaseDate;
  @override
  final List<String> characters;

  @override
  String toString() {
    return 'Film(title: $title, director: $director, producer: $producer, openingCrawl: $openingCrawl, episodeId: $episodeId, releaseDate: $releaseDate, characters: $characters)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Film &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.director, director) ||
                const DeepCollectionEquality()
                    .equals(other.director, director)) &&
            (identical(other.producer, producer) ||
                const DeepCollectionEquality()
                    .equals(other.producer, producer)) &&
            (identical(other.openingCrawl, openingCrawl) ||
                const DeepCollectionEquality()
                    .equals(other.openingCrawl, openingCrawl)) &&
            (identical(other.episodeId, episodeId) ||
                const DeepCollectionEquality()
                    .equals(other.episodeId, episodeId)) &&
            (identical(other.releaseDate, releaseDate) ||
                const DeepCollectionEquality()
                    .equals(other.releaseDate, releaseDate)) &&
            (identical(other.characters, characters) ||
                const DeepCollectionEquality()
                    .equals(other.characters, characters)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(director) ^
      const DeepCollectionEquality().hash(producer) ^
      const DeepCollectionEquality().hash(openingCrawl) ^
      const DeepCollectionEquality().hash(episodeId) ^
      const DeepCollectionEquality().hash(releaseDate) ^
      const DeepCollectionEquality().hash(characters);

  @JsonKey(ignore: true)
  @override
  _$FilmCopyWith<_Film> get copyWith =>
      __$FilmCopyWithImpl<_Film>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_FilmToJson(this);
  }
}

abstract class _Film extends Film {
  const factory _Film(
      {required String title,
      required String director,
      required String producer,
      @JsonKey(name: 'opening_crawl') required String openingCrawl,
      @JsonKey(name: 'episode_id') required int episodeId,
      @JsonKey(name: 'release_date') required DateTime releaseDate,
      required List<String> characters}) = _$_Film;
  const _Film._() : super._();

  factory _Film.fromJson(Map<String, dynamic> json) = _$_Film.fromJson;

  @override
  String get title => throw _privateConstructorUsedError;
  @override
  String get director => throw _privateConstructorUsedError;
  @override
  String get producer => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'opening_crawl')
  String get openingCrawl => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'episode_id')
  int get episodeId => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'release_date')
  DateTime get releaseDate => throw _privateConstructorUsedError;
  @override
  List<String> get characters => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$FilmCopyWith<_Film> get copyWith => throw _privateConstructorUsedError;
}
