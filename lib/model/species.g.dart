// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'species.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Species _$_$_SpeciesFromJson(Map<String, dynamic> json) {
  return _$_Species(
    name: json['name'] as String,
    classification: json['classification'] as String,
    designation: json['designation'] as String,
    hairColor: json['hair_colors'] as String,
    skinColor: json['skin_colors'] as String,
    eyeColor: json['eye_colors'] as String,
    averageLifespan: json['average_lifespan'] as String,
    averageHeight: json['average_height'] as String,
    language: json['language'] as String,
    homeworld: json['homeworld'] as String?,
  );
}

Map<String, dynamic> _$_$_SpeciesToJson(_$_Species instance) =>
    <String, dynamic>{
      'name': instance.name,
      'classification': instance.classification,
      'designation': instance.designation,
      'hair_colors': instance.hairColor,
      'skin_colors': instance.skinColor,
      'eye_colors': instance.eyeColor,
      'average_lifespan': instance.averageLifespan,
      'average_height': instance.averageHeight,
      'language': instance.language,
      'homeworld': instance.homeworld,
    };
