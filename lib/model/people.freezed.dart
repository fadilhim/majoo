// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'people.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

People _$PeopleFromJson(Map<String, dynamic> json) {
  return _People.fromJson(json);
}

/// @nodoc
class _$PeopleTearOff {
  const _$PeopleTearOff();

  _People call(
      {required String name,
      required String height,
      required String mass,
      @JsonKey(name: 'url') required String id,
      @JsonKey(name: 'hair_color') required String hairColor,
      @JsonKey(name: 'skin_color') required String skinColor,
      @JsonKey(name: 'eye_color') required String eyeColor,
      @JsonKey(name: 'birth_year') required String birthYear,
      required String gender,
      required List<String> films,
      required List<String> species}) {
    return _People(
      name: name,
      height: height,
      mass: mass,
      id: id,
      hairColor: hairColor,
      skinColor: skinColor,
      eyeColor: eyeColor,
      birthYear: birthYear,
      gender: gender,
      films: films,
      species: species,
    );
  }

  People fromJson(Map<String, Object> json) {
    return People.fromJson(json);
  }
}

/// @nodoc
const $People = _$PeopleTearOff();

/// @nodoc
mixin _$People {
  String get name => throw _privateConstructorUsedError;
  String get height => throw _privateConstructorUsedError;
  String get mass => throw _privateConstructorUsedError;
  @JsonKey(name: 'url')
  String get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'hair_color')
  String get hairColor => throw _privateConstructorUsedError;
  @JsonKey(name: 'skin_color')
  String get skinColor => throw _privateConstructorUsedError;
  @JsonKey(name: 'eye_color')
  String get eyeColor => throw _privateConstructorUsedError;
  @JsonKey(name: 'birth_year')
  String get birthYear => throw _privateConstructorUsedError;
  String get gender => throw _privateConstructorUsedError;
  List<String> get films => throw _privateConstructorUsedError;
  List<String> get species => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PeopleCopyWith<People> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PeopleCopyWith<$Res> {
  factory $PeopleCopyWith(People value, $Res Function(People) then) =
      _$PeopleCopyWithImpl<$Res>;
  $Res call(
      {String name,
      String height,
      String mass,
      @JsonKey(name: 'url') String id,
      @JsonKey(name: 'hair_color') String hairColor,
      @JsonKey(name: 'skin_color') String skinColor,
      @JsonKey(name: 'eye_color') String eyeColor,
      @JsonKey(name: 'birth_year') String birthYear,
      String gender,
      List<String> films,
      List<String> species});
}

/// @nodoc
class _$PeopleCopyWithImpl<$Res> implements $PeopleCopyWith<$Res> {
  _$PeopleCopyWithImpl(this._value, this._then);

  final People _value;
  // ignore: unused_field
  final $Res Function(People) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? height = freezed,
    Object? mass = freezed,
    Object? id = freezed,
    Object? hairColor = freezed,
    Object? skinColor = freezed,
    Object? eyeColor = freezed,
    Object? birthYear = freezed,
    Object? gender = freezed,
    Object? films = freezed,
    Object? species = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      height: height == freezed
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as String,
      mass: mass == freezed
          ? _value.mass
          : mass // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      hairColor: hairColor == freezed
          ? _value.hairColor
          : hairColor // ignore: cast_nullable_to_non_nullable
              as String,
      skinColor: skinColor == freezed
          ? _value.skinColor
          : skinColor // ignore: cast_nullable_to_non_nullable
              as String,
      eyeColor: eyeColor == freezed
          ? _value.eyeColor
          : eyeColor // ignore: cast_nullable_to_non_nullable
              as String,
      birthYear: birthYear == freezed
          ? _value.birthYear
          : birthYear // ignore: cast_nullable_to_non_nullable
              as String,
      gender: gender == freezed
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      films: films == freezed
          ? _value.films
          : films // ignore: cast_nullable_to_non_nullable
              as List<String>,
      species: species == freezed
          ? _value.species
          : species // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
abstract class _$PeopleCopyWith<$Res> implements $PeopleCopyWith<$Res> {
  factory _$PeopleCopyWith(_People value, $Res Function(_People) then) =
      __$PeopleCopyWithImpl<$Res>;
  @override
  $Res call(
      {String name,
      String height,
      String mass,
      @JsonKey(name: 'url') String id,
      @JsonKey(name: 'hair_color') String hairColor,
      @JsonKey(name: 'skin_color') String skinColor,
      @JsonKey(name: 'eye_color') String eyeColor,
      @JsonKey(name: 'birth_year') String birthYear,
      String gender,
      List<String> films,
      List<String> species});
}

/// @nodoc
class __$PeopleCopyWithImpl<$Res> extends _$PeopleCopyWithImpl<$Res>
    implements _$PeopleCopyWith<$Res> {
  __$PeopleCopyWithImpl(_People _value, $Res Function(_People) _then)
      : super(_value, (v) => _then(v as _People));

  @override
  _People get _value => super._value as _People;

  @override
  $Res call({
    Object? name = freezed,
    Object? height = freezed,
    Object? mass = freezed,
    Object? id = freezed,
    Object? hairColor = freezed,
    Object? skinColor = freezed,
    Object? eyeColor = freezed,
    Object? birthYear = freezed,
    Object? gender = freezed,
    Object? films = freezed,
    Object? species = freezed,
  }) {
    return _then(_People(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      height: height == freezed
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as String,
      mass: mass == freezed
          ? _value.mass
          : mass // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      hairColor: hairColor == freezed
          ? _value.hairColor
          : hairColor // ignore: cast_nullable_to_non_nullable
              as String,
      skinColor: skinColor == freezed
          ? _value.skinColor
          : skinColor // ignore: cast_nullable_to_non_nullable
              as String,
      eyeColor: eyeColor == freezed
          ? _value.eyeColor
          : eyeColor // ignore: cast_nullable_to_non_nullable
              as String,
      birthYear: birthYear == freezed
          ? _value.birthYear
          : birthYear // ignore: cast_nullable_to_non_nullable
              as String,
      gender: gender == freezed
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      films: films == freezed
          ? _value.films
          : films // ignore: cast_nullable_to_non_nullable
              as List<String>,
      species: species == freezed
          ? _value.species
          : species // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_People extends _People {
  const _$_People(
      {required this.name,
      required this.height,
      required this.mass,
      @JsonKey(name: 'url') required this.id,
      @JsonKey(name: 'hair_color') required this.hairColor,
      @JsonKey(name: 'skin_color') required this.skinColor,
      @JsonKey(name: 'eye_color') required this.eyeColor,
      @JsonKey(name: 'birth_year') required this.birthYear,
      required this.gender,
      required this.films,
      required this.species})
      : super._();

  factory _$_People.fromJson(Map<String, dynamic> json) =>
      _$_$_PeopleFromJson(json);

  @override
  final String name;
  @override
  final String height;
  @override
  final String mass;
  @override
  @JsonKey(name: 'url')
  final String id;
  @override
  @JsonKey(name: 'hair_color')
  final String hairColor;
  @override
  @JsonKey(name: 'skin_color')
  final String skinColor;
  @override
  @JsonKey(name: 'eye_color')
  final String eyeColor;
  @override
  @JsonKey(name: 'birth_year')
  final String birthYear;
  @override
  final String gender;
  @override
  final List<String> films;
  @override
  final List<String> species;

  @override
  String toString() {
    return 'People(name: $name, height: $height, mass: $mass, id: $id, hairColor: $hairColor, skinColor: $skinColor, eyeColor: $eyeColor, birthYear: $birthYear, gender: $gender, films: $films, species: $species)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _People &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.height, height) ||
                const DeepCollectionEquality().equals(other.height, height)) &&
            (identical(other.mass, mass) ||
                const DeepCollectionEquality().equals(other.mass, mass)) &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.hairColor, hairColor) ||
                const DeepCollectionEquality()
                    .equals(other.hairColor, hairColor)) &&
            (identical(other.skinColor, skinColor) ||
                const DeepCollectionEquality()
                    .equals(other.skinColor, skinColor)) &&
            (identical(other.eyeColor, eyeColor) ||
                const DeepCollectionEquality()
                    .equals(other.eyeColor, eyeColor)) &&
            (identical(other.birthYear, birthYear) ||
                const DeepCollectionEquality()
                    .equals(other.birthYear, birthYear)) &&
            (identical(other.gender, gender) ||
                const DeepCollectionEquality().equals(other.gender, gender)) &&
            (identical(other.films, films) ||
                const DeepCollectionEquality().equals(other.films, films)) &&
            (identical(other.species, species) ||
                const DeepCollectionEquality().equals(other.species, species)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(height) ^
      const DeepCollectionEquality().hash(mass) ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(hairColor) ^
      const DeepCollectionEquality().hash(skinColor) ^
      const DeepCollectionEquality().hash(eyeColor) ^
      const DeepCollectionEquality().hash(birthYear) ^
      const DeepCollectionEquality().hash(gender) ^
      const DeepCollectionEquality().hash(films) ^
      const DeepCollectionEquality().hash(species);

  @JsonKey(ignore: true)
  @override
  _$PeopleCopyWith<_People> get copyWith =>
      __$PeopleCopyWithImpl<_People>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_PeopleToJson(this);
  }
}

abstract class _People extends People {
  const factory _People(
      {required String name,
      required String height,
      required String mass,
      @JsonKey(name: 'url') required String id,
      @JsonKey(name: 'hair_color') required String hairColor,
      @JsonKey(name: 'skin_color') required String skinColor,
      @JsonKey(name: 'eye_color') required String eyeColor,
      @JsonKey(name: 'birth_year') required String birthYear,
      required String gender,
      required List<String> films,
      required List<String> species}) = _$_People;
  const _People._() : super._();

  factory _People.fromJson(Map<String, dynamic> json) = _$_People.fromJson;

  @override
  String get name => throw _privateConstructorUsedError;
  @override
  String get height => throw _privateConstructorUsedError;
  @override
  String get mass => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'url')
  String get id => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'hair_color')
  String get hairColor => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'skin_color')
  String get skinColor => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'eye_color')
  String get eyeColor => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'birth_year')
  String get birthYear => throw _privateConstructorUsedError;
  @override
  String get gender => throw _privateConstructorUsedError;
  @override
  List<String> get films => throw _privateConstructorUsedError;
  @override
  List<String> get species => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$PeopleCopyWith<_People> get copyWith => throw _privateConstructorUsedError;
}
