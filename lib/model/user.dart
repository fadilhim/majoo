import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';

part 'user.g.dart';

@freezed
class User with _$User {
  static const fromJsonFactory = _$UserFromJson;

  const User._();

  const factory User({
    required String email,
    required String name,
    required String password,
    required int age,
    required int height,
    required int mass,
  }) = _User;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}
