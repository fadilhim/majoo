// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'people.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_People _$_$_PeopleFromJson(Map<String, dynamic> json) {
  return _$_People(
    name: json['name'] as String,
    height: json['height'] as String,
    mass: json['mass'] as String,
    id: json['url'] as String,
    hairColor: json['hair_color'] as String,
    skinColor: json['skin_color'] as String,
    eyeColor: json['eye_color'] as String,
    birthYear: json['birth_year'] as String,
    gender: json['gender'] as String,
    films: (json['films'] as List<dynamic>).map((e) => e as String).toList(),
    species:
        (json['species'] as List<dynamic>).map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$_$_PeopleToJson(_$_People instance) => <String, dynamic>{
      'name': instance.name,
      'height': instance.height,
      'mass': instance.mass,
      'url': instance.id,
      'hair_color': instance.hairColor,
      'skin_color': instance.skinColor,
      'eye_color': instance.eyeColor,
      'birth_year': instance.birthYear,
      'gender': instance.gender,
      'films': instance.films,
      'species': instance.species,
    };
