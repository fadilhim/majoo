import 'package:freezed_annotation/freezed_annotation.dart';

part 'species.freezed.dart';

part 'species.g.dart';

@freezed
class Species with _$Species {
  static const fromJsonFactory = _$SpeciesFromJson;

  const Species._();

  const factory Species({
    required String name,
    required String classification,
    required String designation,
    @JsonKey(name: 'hair_colors') required String hairColor,
    @JsonKey(name: 'skin_colors') required String skinColor,
    @JsonKey(name: 'eye_colors') required String eyeColor,
    @JsonKey(name: 'average_lifespan') required String averageLifespan,
    @JsonKey(name: 'average_height') required String averageHeight,
    required String language,
    String? homeworld,
  }) = _Species;

  factory Species.fromJson(Map<String, dynamic> json) => _$SpeciesFromJson(json);
}
