import 'package:freezed_annotation/freezed_annotation.dart';

part 'db_type.freezed.dart';

part 'db_type.g.dart';

@freezed
class DBType with _$DBType {
  static const fromJsonFactory = _$DBTypeFromJson;

  const DBType._();

  const factory DBType({
    required String id,
    required String content,
    String? name,
  }) = _DBType;

  factory DBType.fromJson(Map<String, dynamic> json) => _$DBTypeFromJson(json);
}
