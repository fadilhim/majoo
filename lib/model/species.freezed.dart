// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'species.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Species _$SpeciesFromJson(Map<String, dynamic> json) {
  return _Species.fromJson(json);
}

/// @nodoc
class _$SpeciesTearOff {
  const _$SpeciesTearOff();

  _Species call(
      {required String name,
      required String classification,
      required String designation,
      @JsonKey(name: 'hair_colors') required String hairColor,
      @JsonKey(name: 'skin_colors') required String skinColor,
      @JsonKey(name: 'eye_colors') required String eyeColor,
      @JsonKey(name: 'average_lifespan') required String averageLifespan,
      @JsonKey(name: 'average_height') required String averageHeight,
      required String language,
      String? homeworld}) {
    return _Species(
      name: name,
      classification: classification,
      designation: designation,
      hairColor: hairColor,
      skinColor: skinColor,
      eyeColor: eyeColor,
      averageLifespan: averageLifespan,
      averageHeight: averageHeight,
      language: language,
      homeworld: homeworld,
    );
  }

  Species fromJson(Map<String, Object> json) {
    return Species.fromJson(json);
  }
}

/// @nodoc
const $Species = _$SpeciesTearOff();

/// @nodoc
mixin _$Species {
  String get name => throw _privateConstructorUsedError;
  String get classification => throw _privateConstructorUsedError;
  String get designation => throw _privateConstructorUsedError;
  @JsonKey(name: 'hair_colors')
  String get hairColor => throw _privateConstructorUsedError;
  @JsonKey(name: 'skin_colors')
  String get skinColor => throw _privateConstructorUsedError;
  @JsonKey(name: 'eye_colors')
  String get eyeColor => throw _privateConstructorUsedError;
  @JsonKey(name: 'average_lifespan')
  String get averageLifespan => throw _privateConstructorUsedError;
  @JsonKey(name: 'average_height')
  String get averageHeight => throw _privateConstructorUsedError;
  String get language => throw _privateConstructorUsedError;
  String? get homeworld => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SpeciesCopyWith<Species> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SpeciesCopyWith<$Res> {
  factory $SpeciesCopyWith(Species value, $Res Function(Species) then) =
      _$SpeciesCopyWithImpl<$Res>;
  $Res call(
      {String name,
      String classification,
      String designation,
      @JsonKey(name: 'hair_colors') String hairColor,
      @JsonKey(name: 'skin_colors') String skinColor,
      @JsonKey(name: 'eye_colors') String eyeColor,
      @JsonKey(name: 'average_lifespan') String averageLifespan,
      @JsonKey(name: 'average_height') String averageHeight,
      String language,
      String? homeworld});
}

/// @nodoc
class _$SpeciesCopyWithImpl<$Res> implements $SpeciesCopyWith<$Res> {
  _$SpeciesCopyWithImpl(this._value, this._then);

  final Species _value;
  // ignore: unused_field
  final $Res Function(Species) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? classification = freezed,
    Object? designation = freezed,
    Object? hairColor = freezed,
    Object? skinColor = freezed,
    Object? eyeColor = freezed,
    Object? averageLifespan = freezed,
    Object? averageHeight = freezed,
    Object? language = freezed,
    Object? homeworld = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      classification: classification == freezed
          ? _value.classification
          : classification // ignore: cast_nullable_to_non_nullable
              as String,
      designation: designation == freezed
          ? _value.designation
          : designation // ignore: cast_nullable_to_non_nullable
              as String,
      hairColor: hairColor == freezed
          ? _value.hairColor
          : hairColor // ignore: cast_nullable_to_non_nullable
              as String,
      skinColor: skinColor == freezed
          ? _value.skinColor
          : skinColor // ignore: cast_nullable_to_non_nullable
              as String,
      eyeColor: eyeColor == freezed
          ? _value.eyeColor
          : eyeColor // ignore: cast_nullable_to_non_nullable
              as String,
      averageLifespan: averageLifespan == freezed
          ? _value.averageLifespan
          : averageLifespan // ignore: cast_nullable_to_non_nullable
              as String,
      averageHeight: averageHeight == freezed
          ? _value.averageHeight
          : averageHeight // ignore: cast_nullable_to_non_nullable
              as String,
      language: language == freezed
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String,
      homeworld: homeworld == freezed
          ? _value.homeworld
          : homeworld // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$SpeciesCopyWith<$Res> implements $SpeciesCopyWith<$Res> {
  factory _$SpeciesCopyWith(_Species value, $Res Function(_Species) then) =
      __$SpeciesCopyWithImpl<$Res>;
  @override
  $Res call(
      {String name,
      String classification,
      String designation,
      @JsonKey(name: 'hair_colors') String hairColor,
      @JsonKey(name: 'skin_colors') String skinColor,
      @JsonKey(name: 'eye_colors') String eyeColor,
      @JsonKey(name: 'average_lifespan') String averageLifespan,
      @JsonKey(name: 'average_height') String averageHeight,
      String language,
      String? homeworld});
}

/// @nodoc
class __$SpeciesCopyWithImpl<$Res> extends _$SpeciesCopyWithImpl<$Res>
    implements _$SpeciesCopyWith<$Res> {
  __$SpeciesCopyWithImpl(_Species _value, $Res Function(_Species) _then)
      : super(_value, (v) => _then(v as _Species));

  @override
  _Species get _value => super._value as _Species;

  @override
  $Res call({
    Object? name = freezed,
    Object? classification = freezed,
    Object? designation = freezed,
    Object? hairColor = freezed,
    Object? skinColor = freezed,
    Object? eyeColor = freezed,
    Object? averageLifespan = freezed,
    Object? averageHeight = freezed,
    Object? language = freezed,
    Object? homeworld = freezed,
  }) {
    return _then(_Species(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      classification: classification == freezed
          ? _value.classification
          : classification // ignore: cast_nullable_to_non_nullable
              as String,
      designation: designation == freezed
          ? _value.designation
          : designation // ignore: cast_nullable_to_non_nullable
              as String,
      hairColor: hairColor == freezed
          ? _value.hairColor
          : hairColor // ignore: cast_nullable_to_non_nullable
              as String,
      skinColor: skinColor == freezed
          ? _value.skinColor
          : skinColor // ignore: cast_nullable_to_non_nullable
              as String,
      eyeColor: eyeColor == freezed
          ? _value.eyeColor
          : eyeColor // ignore: cast_nullable_to_non_nullable
              as String,
      averageLifespan: averageLifespan == freezed
          ? _value.averageLifespan
          : averageLifespan // ignore: cast_nullable_to_non_nullable
              as String,
      averageHeight: averageHeight == freezed
          ? _value.averageHeight
          : averageHeight // ignore: cast_nullable_to_non_nullable
              as String,
      language: language == freezed
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String,
      homeworld: homeworld == freezed
          ? _value.homeworld
          : homeworld // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Species extends _Species {
  const _$_Species(
      {required this.name,
      required this.classification,
      required this.designation,
      @JsonKey(name: 'hair_colors') required this.hairColor,
      @JsonKey(name: 'skin_colors') required this.skinColor,
      @JsonKey(name: 'eye_colors') required this.eyeColor,
      @JsonKey(name: 'average_lifespan') required this.averageLifespan,
      @JsonKey(name: 'average_height') required this.averageHeight,
      required this.language,
      this.homeworld})
      : super._();

  factory _$_Species.fromJson(Map<String, dynamic> json) =>
      _$_$_SpeciesFromJson(json);

  @override
  final String name;
  @override
  final String classification;
  @override
  final String designation;
  @override
  @JsonKey(name: 'hair_colors')
  final String hairColor;
  @override
  @JsonKey(name: 'skin_colors')
  final String skinColor;
  @override
  @JsonKey(name: 'eye_colors')
  final String eyeColor;
  @override
  @JsonKey(name: 'average_lifespan')
  final String averageLifespan;
  @override
  @JsonKey(name: 'average_height')
  final String averageHeight;
  @override
  final String language;
  @override
  final String? homeworld;

  @override
  String toString() {
    return 'Species(name: $name, classification: $classification, designation: $designation, hairColor: $hairColor, skinColor: $skinColor, eyeColor: $eyeColor, averageLifespan: $averageLifespan, averageHeight: $averageHeight, language: $language, homeworld: $homeworld)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Species &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.classification, classification) ||
                const DeepCollectionEquality()
                    .equals(other.classification, classification)) &&
            (identical(other.designation, designation) ||
                const DeepCollectionEquality()
                    .equals(other.designation, designation)) &&
            (identical(other.hairColor, hairColor) ||
                const DeepCollectionEquality()
                    .equals(other.hairColor, hairColor)) &&
            (identical(other.skinColor, skinColor) ||
                const DeepCollectionEquality()
                    .equals(other.skinColor, skinColor)) &&
            (identical(other.eyeColor, eyeColor) ||
                const DeepCollectionEquality()
                    .equals(other.eyeColor, eyeColor)) &&
            (identical(other.averageLifespan, averageLifespan) ||
                const DeepCollectionEquality()
                    .equals(other.averageLifespan, averageLifespan)) &&
            (identical(other.averageHeight, averageHeight) ||
                const DeepCollectionEquality()
                    .equals(other.averageHeight, averageHeight)) &&
            (identical(other.language, language) ||
                const DeepCollectionEquality()
                    .equals(other.language, language)) &&
            (identical(other.homeworld, homeworld) ||
                const DeepCollectionEquality()
                    .equals(other.homeworld, homeworld)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(classification) ^
      const DeepCollectionEquality().hash(designation) ^
      const DeepCollectionEquality().hash(hairColor) ^
      const DeepCollectionEquality().hash(skinColor) ^
      const DeepCollectionEquality().hash(eyeColor) ^
      const DeepCollectionEquality().hash(averageLifespan) ^
      const DeepCollectionEquality().hash(averageHeight) ^
      const DeepCollectionEquality().hash(language) ^
      const DeepCollectionEquality().hash(homeworld);

  @JsonKey(ignore: true)
  @override
  _$SpeciesCopyWith<_Species> get copyWith =>
      __$SpeciesCopyWithImpl<_Species>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_SpeciesToJson(this);
  }
}

abstract class _Species extends Species {
  const factory _Species(
      {required String name,
      required String classification,
      required String designation,
      @JsonKey(name: 'hair_colors') required String hairColor,
      @JsonKey(name: 'skin_colors') required String skinColor,
      @JsonKey(name: 'eye_colors') required String eyeColor,
      @JsonKey(name: 'average_lifespan') required String averageLifespan,
      @JsonKey(name: 'average_height') required String averageHeight,
      required String language,
      String? homeworld}) = _$_Species;
  const _Species._() : super._();

  factory _Species.fromJson(Map<String, dynamic> json) = _$_Species.fromJson;

  @override
  String get name => throw _privateConstructorUsedError;
  @override
  String get classification => throw _privateConstructorUsedError;
  @override
  String get designation => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'hair_colors')
  String get hairColor => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'skin_colors')
  String get skinColor => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'eye_colors')
  String get eyeColor => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'average_lifespan')
  String get averageLifespan => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'average_height')
  String get averageHeight => throw _privateConstructorUsedError;
  @override
  String get language => throw _privateConstructorUsedError;
  @override
  String? get homeworld => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$SpeciesCopyWith<_Species> get copyWith =>
      throw _privateConstructorUsedError;
}
