import 'package:freezed_annotation/freezed_annotation.dart';

part 'people.freezed.dart';

part 'people.g.dart';

@freezed
class People with _$People {
  static const fromJsonFactory = _$PeopleFromJson;

  const People._();

  const factory People({
    required String name,
    required String height,
    required String mass,
    @JsonKey(name: 'url') required String id,
    @JsonKey(name: 'hair_color') required String hairColor,
    @JsonKey(name: 'skin_color') required String skinColor,
    @JsonKey(name: 'eye_color') required String eyeColor,
    @JsonKey(name: 'birth_year') required String birthYear,
    required String gender,
    required List<String> films,
    required List<String> species,
  }) = _People;

  factory People.fromJson(Map<String, dynamic> json) => _$PeopleFromJson(json);
}
