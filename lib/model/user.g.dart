// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_User _$_$_UserFromJson(Map<String, dynamic> json) {
  return _$_User(
    email: json['email'] as String,
    name: json['name'] as String,
    password: json['password'] as String,
    age: json['age'] as int,
    height: json['height'] as int,
    mass: json['mass'] as int,
  );
}

Map<String, dynamic> _$_$_UserToJson(_$_User instance) => <String, dynamic>{
      'email': instance.email,
      'name': instance.name,
      'password': instance.password,
      'age': instance.age,
      'height': instance.height,
      'mass': instance.mass,
    };
