// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'db_type.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_DBType _$_$_DBTypeFromJson(Map<String, dynamic> json) {
  return _$_DBType(
    id: json['id'] as String,
    content: json['content'] as String,
    name: json['name'] as String?,
  );
}

Map<String, dynamic> _$_$_DBTypeToJson(_$_DBType instance) => <String, dynamic>{
      'id': instance.id,
      'content': instance.content,
      'name': instance.name,
    };
