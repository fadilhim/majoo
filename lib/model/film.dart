import 'package:freezed_annotation/freezed_annotation.dart';

part 'film.freezed.dart';

part 'film.g.dart';

@freezed
class Film with _$Film {
  static const fromJsonFactory = _$FilmFromJson;

  const Film._();

  const factory Film({
    required String title,
    required String director,
    required String producer,
    @JsonKey(name: 'opening_crawl') required String openingCrawl,
    @JsonKey(name: 'episode_id') required int episodeId,
    @JsonKey(name: 'release_date') required DateTime releaseDate,
    required List<String> characters,
  }) = _Film;

  factory Film.fromJson(Map<String, dynamic> json) => _$FilmFromJson(json);
}
