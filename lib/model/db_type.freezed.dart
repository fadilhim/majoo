// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'db_type.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

DBType _$DBTypeFromJson(Map<String, dynamic> json) {
  return _DBType.fromJson(json);
}

/// @nodoc
class _$DBTypeTearOff {
  const _$DBTypeTearOff();

  _DBType call({required String id, required String content, String? name}) {
    return _DBType(
      id: id,
      content: content,
      name: name,
    );
  }

  DBType fromJson(Map<String, Object> json) {
    return DBType.fromJson(json);
  }
}

/// @nodoc
const $DBType = _$DBTypeTearOff();

/// @nodoc
mixin _$DBType {
  String get id => throw _privateConstructorUsedError;
  String get content => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DBTypeCopyWith<DBType> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DBTypeCopyWith<$Res> {
  factory $DBTypeCopyWith(DBType value, $Res Function(DBType) then) =
      _$DBTypeCopyWithImpl<$Res>;
  $Res call({String id, String content, String? name});
}

/// @nodoc
class _$DBTypeCopyWithImpl<$Res> implements $DBTypeCopyWith<$Res> {
  _$DBTypeCopyWithImpl(this._value, this._then);

  final DBType _value;
  // ignore: unused_field
  final $Res Function(DBType) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? content = freezed,
    Object? name = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      content: content == freezed
          ? _value.content
          : content // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$DBTypeCopyWith<$Res> implements $DBTypeCopyWith<$Res> {
  factory _$DBTypeCopyWith(_DBType value, $Res Function(_DBType) then) =
      __$DBTypeCopyWithImpl<$Res>;
  @override
  $Res call({String id, String content, String? name});
}

/// @nodoc
class __$DBTypeCopyWithImpl<$Res> extends _$DBTypeCopyWithImpl<$Res>
    implements _$DBTypeCopyWith<$Res> {
  __$DBTypeCopyWithImpl(_DBType _value, $Res Function(_DBType) _then)
      : super(_value, (v) => _then(v as _DBType));

  @override
  _DBType get _value => super._value as _DBType;

  @override
  $Res call({
    Object? id = freezed,
    Object? content = freezed,
    Object? name = freezed,
  }) {
    return _then(_DBType(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      content: content == freezed
          ? _value.content
          : content // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_DBType extends _DBType {
  const _$_DBType({required this.id, required this.content, this.name})
      : super._();

  factory _$_DBType.fromJson(Map<String, dynamic> json) =>
      _$_$_DBTypeFromJson(json);

  @override
  final String id;
  @override
  final String content;
  @override
  final String? name;

  @override
  String toString() {
    return 'DBType(id: $id, content: $content, name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _DBType &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.content, content) ||
                const DeepCollectionEquality()
                    .equals(other.content, content)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(content) ^
      const DeepCollectionEquality().hash(name);

  @JsonKey(ignore: true)
  @override
  _$DBTypeCopyWith<_DBType> get copyWith =>
      __$DBTypeCopyWithImpl<_DBType>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_DBTypeToJson(this);
  }
}

abstract class _DBType extends DBType {
  const factory _DBType(
      {required String id, required String content, String? name}) = _$_DBType;
  const _DBType._() : super._();

  factory _DBType.fromJson(Map<String, dynamic> json) = _$_DBType.fromJson;

  @override
  String get id => throw _privateConstructorUsedError;
  @override
  String get content => throw _privateConstructorUsedError;
  @override
  String? get name => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$DBTypeCopyWith<_DBType> get copyWith => throw _privateConstructorUsedError;
}
