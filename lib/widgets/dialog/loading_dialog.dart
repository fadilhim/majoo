import 'dart:ui';

import 'package:animations/animations.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:majoo/utilities/ui_utils.dart';

class LoadingDialog extends StatefulWidget {
  final String? title;
  final String? message;

  const LoadingDialog._({
    this.title,
    this.message,
  });

  static void showDialog(
      BuildContext context, {
        String? title,
        String? message,
      }) {
    showModal(
        context: context,
        configuration: const FadeScaleTransitionConfiguration(
          barrierDismissible: false,
        ),
        builder: (context) => LoadingDialog._(
          title: title,
          message: message,
        ));
  }

  @override
  _LoadingDialogState createState() => _LoadingDialogState();
}

class _LoadingDialogState extends State<LoadingDialog> {
  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add(onBackPress);
  }

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
      child: Dialog(
        backgroundColor: Colors.transparent,
        child: dialogContent(context),
      ),
    );
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(onBackPress);
    super.dispose();
  }

  bool onBackPress(bool stopDefaultButtonEvent, RouteInfo info) => true;

  Widget dialogContent(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(
        maxWidth: ResponsiveUi.dialogMaxWidth,
      ),
      padding: const EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(16.0),
        boxShadow: const [
          BoxShadow(
            color: Colors.black38,
            blurRadius: 10.0,
            offset: Offset(0.0, 10.0),
          )
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Lottie.asset(
            'lib/assets/lottie/loading.json',
            width: 100,
            height: 100,
            fit: BoxFit.scaleDown,
          ),
          const SizedBox(height: 12.0),
          Text(
            widget.title ?? 'loading',
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: Color(0xff525252),
              fontWeight: FontWeight.w700,
            ),
          ),
          const SizedBox(height: 12.0),
          Text(
            widget.message ??
                'please wait',
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: Color(0xff525252),
              fontWeight: FontWeight.w400,
            ),
          ),
          const SizedBox(height: 12.0),
        ],
      ),
    );
  }
}
