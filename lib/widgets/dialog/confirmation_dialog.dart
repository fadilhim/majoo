import 'dart:ui';

import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:majoo/utilities/ui_utils.dart';

class SimpleConfirmationDialog extends StatefulWidget {
  final String? title;
  final String message;
  final String? leftButtonLabel;
  final String? rightButtonLabel;
  final Image? customImage;
  final VoidCallback? onConfirm;
  final VoidCallback? onCancel;

  const SimpleConfirmationDialog._({
    this.title,
    required this.message,
    this.leftButtonLabel,
    this.rightButtonLabel,
    this.customImage,
    this.onConfirm,
    this.onCancel,
  });

  static void showDialog(
      BuildContext context, {
        String? title,
        required String message,
        String? leftButtonLabel,
        String? rightButtonLabel,
        Image? customImage,
        bool? isOptional,
        VoidCallback? onConfirm,
        VoidCallback? onCancel,
      }) {
    showModal(
        context: context,
        configuration:
        const FadeScaleTransitionConfiguration(barrierDismissible: false),
        builder: (context) => SimpleConfirmationDialog._(
          title: title,
          message: message,
          leftButtonLabel: leftButtonLabel,
          rightButtonLabel: rightButtonLabel,
          customImage: customImage,
          onConfirm: onConfirm,
          onCancel: onCancel,
        ));
  }

  @override
  _SimpleConfirmationDialogState createState() =>
      _SimpleConfirmationDialogState();
}

class _SimpleConfirmationDialogState
    extends State<SimpleConfirmationDialog> {
  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
      child: Dialog(
        backgroundColor: Colors.transparent,
        child: dialogContent(context),
      ),
    );
  }

  Widget dialogContent(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(
        maxWidth: ResponsiveUi.dialogMaxWidth,
      ),
      padding: const EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(16.0),
        boxShadow: const [
          BoxShadow(
            color: Colors.black38,
            blurRadius: 10.0,
            offset: Offset(0.0, 10.0),
          )
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          const SizedBox(height: 12.0),
          Text(
            widget.title ?? 'Confirmation',
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: Color(0xff525252),
              fontWeight: FontWeight.w700,
            ),
          ),
          const SizedBox(height: 12.0),
          Text(
            widget.message,
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: Color(0xff525252),
              fontWeight: FontWeight.w400,
            ),
          ),
          const SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: TextButton(
                  style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        side: BorderSide(color: Colors.grey)),
                    backgroundColor: Colors.white,
                    primary: Theme.of(context).accentColor,
                    padding: const EdgeInsets.all(8.0),
                  ),
                  onPressed: () {
                    widget.onCancel?.call();
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    widget.leftButtonLabel ??
                        'Cancel', style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                flex: 3,
                child: TextButton(
                  style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          side:
                          BorderSide(color: Theme.of(context).accentColor)),
                      backgroundColor: Colors.red,
                      primary: Colors.white),
                  onPressed: () {
                    widget.onConfirm?.call();
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    widget.rightButtonLabel ??
                        'Confirm',
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
