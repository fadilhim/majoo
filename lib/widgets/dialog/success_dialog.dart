import 'dart:ui';

import 'package:animations/animations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lottie/lottie.dart';
import 'package:majoo/utilities/ui_utils.dart';

class SuccessDialog extends StatefulWidget {
  final String title;
  final String message;
  final VoidCallback? onOk;

  const SuccessDialog._({
    required this.title,
    required this.message,
    this.onOk,
  });

  static void showDialog(
    BuildContext context, {
    String title = 'Yeay!',
    required String message,
    VoidCallback? onOk,
  }) {
    showModal(
        context: context,
        configuration:
            const FadeScaleTransitionConfiguration(barrierDismissible: false),
        builder: (context) => SuccessDialog._(
              title: title,
              message: message,
              onOk: onOk,
            ));
  }

  @override
  _SuccessDialogState createState() => _SuccessDialogState();
}

class _SuccessDialogState extends State<SuccessDialog> {
  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
      child: Dialog(
        backgroundColor: Colors.transparent,
        child: dialogContent(context),
      ),
    );
  }

  Widget dialogContent(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
        maxWidth: ResponsiveUi.dialogMaxWidth,
      ),
      padding: const EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(16.0),
        boxShadow: const [
          BoxShadow(
            color: Colors.black38,
            blurRadius: 10.0,
            offset: Offset(0.0, 10.0),
          )
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Lottie.asset('lib/assets/lottie/success.json',
              width: 150, height: 150, fit: BoxFit.scaleDown),
          const SizedBox(height: 12.0),
          Text(
            widget.title,
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: Color(0xff525252),
              fontWeight: FontWeight.w700,
            ),
          ),
          const SizedBox(height: 12.0),
          Text(
            widget.message,
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: Color(0xff525252),
              fontWeight: FontWeight.w400,
            ),
          ),
          const SizedBox(height: 20.0),
          TextButton(
            style: TextButton.styleFrom(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0)),
              backgroundColor: const Color(0xff95989a),
              primary: Colors.white,
              padding: const EdgeInsets.all(8.0),
            ),
            onPressed: () {
              widget.onOk?.call();
              Navigator.pop(context);
              // AutoRouter.of(context).pop();
            },
            child: Text(
              'close',
            ),
          ),
        ],
      ),
    );
  }
}
