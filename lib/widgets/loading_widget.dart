import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Lottie.asset('lib/assets/lottie/loading.json',
          width: 150, height: 150, fit: BoxFit.scaleDown),
    );
  }
}
