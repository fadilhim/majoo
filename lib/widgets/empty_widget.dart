import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class EmptyWidget extends StatelessWidget {
  EmptyWidget({Key? key, String? label}) : super(key: key);

  String? label;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Lottie.asset('lib/assets/lottie/empty.json',
            width: 150, height: 150, fit: BoxFit.scaleDown),
        Text(label ?? 'There\'s no data yet'),
      ]),
    );
  }
}
