import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Widget? titleIcon;
  final Color? titleIconColor;
  final String? header;
  final TextStyle? headerTextStyle;
  final String? title;
  final Widget? customTitle;
  final Widget? leading;
  final bool automaticallyImplyLeading;
  final List<Widget>? actions;
  final Widget? flexibleSpace;
  final PreferredSizeWidget? bottom;
  final double? elevation;
  final Color? shadowColor;
  final ShapeBorder? shape;
  final Color? backgroundColor;
  final Brightness? brightness;
  final IconThemeData? iconTheme;
  final IconThemeData? actionsIconTheme;
  final TextTheme? textTheme;
  final bool primary;
  final bool? centerTitle;
  final bool excludeHeaderSemantics;
  final double titleSpacing;
  final double toolbarOpacity;
  final double bottomOpacity;
  final double? toolbarHeight;
  final bool? backwardsCompatibility;
  final SystemUiOverlayStyle? systemOverlayStyle;

  const CustomAppBar({
    this.backwardsCompatibility,
    this.systemOverlayStyle,
    this.titleIcon,
    this.titleIconColor,
    this.header,
    this.headerTextStyle,
    this.customTitle,
    this.title,
    this.leading,
    this.automaticallyImplyLeading = false,
    this.actions,
    this.flexibleSpace,
    this.bottom,
    this.elevation = 0,
    this.shadowColor,
    this.shape,
    this.backgroundColor = Colors.white,
    this.brightness,
    this.iconTheme,
    this.actionsIconTheme,
    this.textTheme,
    this.primary = true,
    this.centerTitle = false,
    this.excludeHeaderSemantics = false,
    this.titleSpacing = NavigationToolbar.kMiddleSpacing,
    this.toolbarOpacity = 1.0,
    this.bottomOpacity = 1.0,
    this.toolbarHeight,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backwardsCompatibility: backwardsCompatibility,
      systemOverlayStyle: systemOverlayStyle,
      leading: leading != null ? leading : null,
      automaticallyImplyLeading: automaticallyImplyLeading,
      actions: actions,
      bottom: bottom,
      elevation: elevation,
      shadowColor: shadowColor,
      shape: shape,
      backgroundColor: backgroundColor,
      brightness: brightness,
      iconTheme: iconTheme,
      actionsIconTheme: actionsIconTheme,
      textTheme: textTheme,
      primary: primary,
      centerTitle: centerTitle,
      excludeHeaderSemantics: excludeHeaderSemantics,
      titleSpacing: titleSpacing,
      toolbarOpacity: toolbarOpacity,
      bottomOpacity: bottomOpacity,
      toolbarHeight: toolbarHeight,
      title: _createChild(),
      flexibleSpace: flexibleSpace,
    );
  }

  Widget? _createChild() {
    if ((titleIcon != null || header != null)) {
      return _flexibleSpaceBar(
          titleIcon: titleIcon,
          header: header,
          headerTextStyle: headerTextStyle);
    }

    final customTitle = this.customTitle;
    if (customTitle != null) return customTitle;
    final title = this.title;
    return title != null
        ? Text(title, style: TextStyle(color: Colors.black))
        : null;
  }

  Widget _flexibleSpaceBar({
    Widget? titleIcon,
    String? header,
    TextStyle? headerTextStyle,
  }) {
    var _container = <Widget>[];
    var _bar = <Widget>[];
    if (header != null) {
      _container.add(
        Text(
          header,
          style: headerTextStyle,
        ),
      );
    }
    if (titleIcon != null) {
      _bar.addAll([
        Hero(
          tag: titleIcon,
          child: SizedBox(
            width: 20,
            height: 20,
            child: titleIcon,
          ),
        ),
        const SizedBox(
          width: 10,
        ),
      ]);
    }

    final customTitle = this.customTitle;
    if (customTitle != null) {
      _bar.add(Flexible(
        child: customTitle,
      ));
    }

    final title = this.title;
    // if (title != null) {
    //   _bar.add(Flexible(
    //     child: Text(
    //       title.toUpperCase(),
    //       maxLines: 1,
    //       overflow: TextOverflow.ellipsis,
    //     ),
    //   ));
    // }
    _container.add(
      Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: _bar,
      ),
    );

    return Column(mainAxisSize: MainAxisSize.min, children: _container);
  }

  @override
  Size get preferredSize => Size.fromHeight(
      toolbarHeight ?? kToolbarHeight + (bottom?.preferredSize.height ?? 0.0));
}
