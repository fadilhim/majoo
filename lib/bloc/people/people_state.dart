part of 'people_cubit.dart';

@immutable
abstract class PeopleState {}

class PeopleInitial extends PeopleState {}

class PeopleLoadInProgress extends PeopleState {}

class PeopleLoadSuccess extends PeopleState {}

class EditPeopleLoadSuccess extends PeopleState {}

class CreatePeopleLoadSuccess extends PeopleState {}

class DeletePeopleLoadSuccess extends PeopleState {}

class SearchPeopleLoadSuccess extends PeopleState {
  final List<People> peopleList;

  SearchPeopleLoadSuccess(this.peopleList);
}

class MaxPeopleLoadSuccess extends PeopleState {}

class ChangeViewLoadSuccess extends PeopleState {}

class PeopleLoadFailure extends PeopleState {
  final ErrorMessage error;

  PeopleLoadFailure(this.error);
}