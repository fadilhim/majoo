import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:majoo/main.dart';
import 'package:majoo/model/people.dart';
import 'package:majoo/repositories/favorite_repository.dart';
import 'package:majoo/repositories/people_repository.dart';
import 'package:majoo/utilities/error_message.dart';
import 'package:meta/meta.dart';

part 'people_state.dart';

class PeopleCubit extends Cubit<PeopleState> {
  static PeopleCubit create(BuildContext context) =>
      PeopleCubit._(injector.get(), injector.get());

  PeopleCubit._(this._peopleRepository, this._favoriteRepository)
      : super(PeopleInitial());

  final PeopleRepository _peopleRepository;
  final FavoriteRepository _favoriteRepository;
  final _peopleController = StreamController<List<People>>.broadcast();
  StreamSubscription? favoriteSubscription;
  List<String> favorites = [];
  ViewMode viewMode = ViewMode.list;
  bool isSearchMode = false;
  int _page = 1;
  int _maxPage = 9;

  Stream<List<People>> get peoples => _peopleController.stream;

  changeView() {
    switch (viewMode) {
      case ViewMode.grid:
        viewMode = ViewMode.list;
        break;
      case ViewMode.list:
        viewMode = ViewMode.grid;
        break;
    }
    emit(ChangeViewLoadSuccess());
  }

  initGet() async {
    try {
      emit(PeopleLoadInProgress());
      _page = 0;
      isSearchMode = false;

      final _localResult = await _peopleRepository.getAllLocal();
      if (_localResult.isNotEmpty) {
        final isModulo = ((_localResult.length) % 10) > 0;
        _page = ((_localResult.length) / 10).ceil() + (isModulo ? 1 : 0);

        _peopleController.sink.add(_localResult);
      } else {
        final _result = await _peopleRepository.getFromAPI();

        _peopleRepository.insertAll(_result);
        _peopleController.sink.add(_result);
      }

      favoriteSubscription?.cancel();
      favorites = await _favoriteRepository.getAll();
      favoriteSubscription =
          _favoriteRepository.watchFavoriteList().listen((event) {
        favorites = event;
      });

      emit(PeopleLoadSuccess());
    } catch (e) {
      emit(PeopleLoadFailure(ErrorMessage(e)));
    }
  }

  loadMore() async {
    try {
      if (_page == _maxPage || isSearchMode) {
        emit(MaxPeopleLoadSuccess());
        return;
      }
      _page += 1;

      final _result = await _peopleRepository.getFromAPI(page: _page);

      _peopleRepository.insertAll(_result);
      final res = await _peopleRepository.getAllLocal();
      _peopleController.sink.add(res);

      emit(PeopleLoadSuccess());
    } catch (e) {
      emit(PeopleLoadFailure(ErrorMessage(e)));
    }
  }

  search({required String name}) async {
    isSearchMode = true;
    final result = await _peopleRepository.getAllLocal();
    final filtered = result
        .where((element) =>
            element.name.toLowerCase().contains(name.toLowerCase()))
        .toList();

    _peopleController.sink.add(filtered);
  }

  add({
    required String name,
    required String height,
    required String mass,
    required String hairColor,
    required String skinColor,
    required String eyeColor,
    required String birthYear,
  }) async {
    await _peopleRepository.insert(People(
      name: name,
      height: height,
      mass: mass,
      id: new DateTime.now().millisecondsSinceEpoch.hashCode.toString(),
      hairColor: hairColor,
      skinColor: skinColor,
      eyeColor: eyeColor,
      birthYear: birthYear,
      gender: 'male',
      films: [],
      species: [],
    ));

    emit(CreatePeopleLoadSuccess());

    _updateController();
  }

  delete(People people) async {
    await _peopleRepository.deletePeopleById(people.id);

    if (favorites.contains(people.id)) {
      await _favoriteRepository.deleteFavoriteById(people.id);
    }

    emit(DeletePeopleLoadSuccess());

    _updateController();
  }

  edit({
    required People people,
    String? name,
    String? height,
    String? mass,
    bool? isFavorite,
    String? hairColor,
    String? skinColor,
    String? eyeColor,
    String? birthYear,
  }) async {
    await _peopleRepository.update(people.copyWith(
      name: name ?? people.name,
      height: height ?? people.height,
      mass: mass ?? people.mass,
      hairColor: hairColor ?? people.hairColor,
      skinColor: skinColor ?? people.skinColor,
      eyeColor: eyeColor ?? people.eyeColor,
      birthYear: birthYear ?? people.birthYear,
    ));

    final _isFavorite = isFavorite;
    if (_isFavorite != null) {
      if (_isFavorite) {
        await _favoriteRepository.insert(people);
      } else {
        await _favoriteRepository.deleteFavoriteById(people.id);
      }
    } else {
      emit(EditPeopleLoadSuccess());
    }

    _updateController();
  }

  _updateController() async {
    final res = await _peopleRepository.getAllLocal();
    _peopleController.sink.add(res);
  }

  @override
  Future<void> close() {
    _peopleController.close();
    favoriteSubscription?.cancel();
    return super.close();
  }
}

enum ViewMode {
  grid,
  list,
}
