part of 'film_cubit.dart';

@immutable
abstract class FilmState {}

class FilmInitial extends FilmState {}

class FilmLoadInProgress extends FilmState {}

class FilmLoadSuccess extends FilmState {
  final Film film;

  FilmLoadSuccess(this.film);
}

class FilmLoadFailure extends FilmState {
  final ErrorMessage error;

  FilmLoadFailure(this.error);
}