import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:majoo/main.dart';
import 'package:majoo/model/film.dart';
import 'package:majoo/repositories/network/film_service.dart';
import 'package:majoo/utilities/error_message.dart';
import 'package:majoo/utilities/id_utils.dart';
import 'package:majoo/utilities/chopper_extensions.dart';
import 'package:meta/meta.dart';

part 'film_state.dart';

class FilmCubit extends Cubit<FilmState> {
  static FilmCubit create(BuildContext context, String url) =>
      FilmCubit._(injector.get(), url);

  FilmCubit._(this._filmService, this.url) : super(FilmInitial()) {
    _getById(url);
  }

  String url;

  final FilmService _filmService;

  _getById(String url) async {
    try {
      emit(FilmLoadInProgress());

      final id = url.idFormat;

      final result = await _filmService.getFilm(id: id);

      final film = result.bodyNotNull;

      emit(FilmLoadSuccess(film));
    } catch (e, s) {
      emit(FilmLoadFailure(ErrorMessage(e)));
    }
  }
}
