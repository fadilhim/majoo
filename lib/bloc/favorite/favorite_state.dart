part of 'favorite_cubit.dart';

@immutable
abstract class FavoriteState {}

class FavoriteInitial extends FavoriteState {}

class FavoriteLoadInProgress extends FavoriteState {}

class FavoriteLoadSuccess extends FavoriteState {
  final List<People> peopleList;

  FavoriteLoadSuccess(this.peopleList);
}

class FavoriteLoadFailure extends FavoriteState {
  final ErrorMessage error;

  FavoriteLoadFailure(this.error);
}