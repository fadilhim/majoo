import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:majoo/main.dart';
import 'package:majoo/model/people.dart';
import 'package:majoo/repositories/favorite_repository.dart';
import 'package:majoo/repositories/people_repository.dart';
import 'package:majoo/utilities/error_message.dart';
import 'package:meta/meta.dart';

part 'favorite_state.dart';

class FavoriteCubit extends Cubit<FavoriteState> {
  static FavoriteCubit create(BuildContext context) =>
      FavoriteCubit._(injector.get(), injector.get());

  FavoriteCubit._(this._favoriteRepository, this._peopleRepository)
      : super(FavoriteInitial());

  final _favoriteController = StreamController<List<People>>.broadcast();
  final FavoriteRepository _favoriteRepository;
  final PeopleRepository _peopleRepository;
  ViewMode viewMode = ViewMode.grid;
  List<People> peoples = [];
  List<String> favoritesId = [];

  Stream<List<People>> get favorites => _favoriteController.stream;

  changeView() {
    switch (viewMode) {
      case ViewMode.grid:
        viewMode = ViewMode.list;
        break;
      case ViewMode.list:
        viewMode = ViewMode.grid;
        break;
    }
  }

  initGet() async {
    peoples = await _peopleRepository.getAllLocal();
    favoritesId = await _favoriteRepository.getAll();

    final favPeople =
        peoples.where((element) => favoritesId.contains(element.id)).toList();
    _favoriteController.sink.add(favPeople);
  }

  delete(People people) async {
    await _favoriteRepository.deleteFavoriteById(people.id);

    initGet();
  }

  @override
  Future<void> close() {
    _favoriteController.close();
    return super.close();
  }
}

enum ViewMode {
  grid,
  list,
}
