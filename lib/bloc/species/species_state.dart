part of 'species_cubit.dart';

@immutable
abstract class SpeciesState {}

class SpeciesInitial extends SpeciesState {}

class SpeciesLoadInProgress extends SpeciesState {}

class SpeciesLoadSuccess extends SpeciesState {
  final Species species;

  SpeciesLoadSuccess(this.species);
}

class SpeciesLoadFailure extends SpeciesState {
  final ErrorMessage error;

  SpeciesLoadFailure(this.error);
}