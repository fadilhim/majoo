import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:majoo/main.dart';
import 'package:majoo/model/species.dart';
import 'package:majoo/repositories/network/species_service.dart';
import 'package:majoo/utilities/error_message.dart';
import 'package:majoo/utilities/id_utils.dart';
import 'package:majoo/utilities/chopper_extensions.dart';
import 'package:meta/meta.dart';

part 'species_state.dart';

class SpeciesCubit extends Cubit<SpeciesState> {
  static SpeciesCubit create(BuildContext context, String url) =>
      SpeciesCubit._(injector.get(), url);

  SpeciesCubit._(this._speciesService, this.url) : super(SpeciesInitial()) {
    _getById(url);
  }

  String url;

  final SpeciesService _speciesService;

  _getById(String url) async {
    try {
      emit(SpeciesLoadInProgress());

      final id = url.idFormat;
      final result = await _speciesService.getSpecies(id: id);

      final species = result.bodyNotNull;

      emit(SpeciesLoadSuccess(species));
    } catch (e, s) {
      emit(SpeciesLoadFailure(ErrorMessage(e)));
    }
  }
}
