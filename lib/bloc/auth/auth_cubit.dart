import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:majoo/main.dart';
import 'package:majoo/model/user.dart';
import 'package:majoo/repositories/session_repository.dart';
import 'package:majoo/utilities/error_message.dart';
import 'package:meta/meta.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  static AuthCubit create(BuildContext context) => AuthCubit._(injector.get());

  AuthCubit._(this._authRepository) : super(AuthInitial()) {
    _checkSession();
  }

  final SessionRepository _authRepository;

  bool isLoggedIn = false;

  login({required String email, required String password}) async {
    try {
      emit(LoginLoadInProgress());

      await _authRepository.login(email: email, password: password);
      isLoggedIn = true;

      emit(LoginLoadSuccess());
    } catch (e) {
      emit(LoginLoadFailure(ErrorMessage(e)));
    }
  }

  register({
    required String name,
    required String password,
    required String email,
    required int height,
    required int mass,
    required int age,
  }) async {
    try {
      emit(RegisterLoadInProgress());

      await _authRepository.register(
        email: email,
        password: password,
        name: name,
        height: height,
        age: age,
        mass: mass,
      );

      emit(RegisterLoadSuccess());

      login(email: email, password: password);
    } catch (e) {
      emit(RegisterLoadFailure(ErrorMessage(e)));
    }
  }

  _checkSession() async {
    final user = await _authRepository.getLoggedUser();
    if (user != null) {
      isLoggedIn = true;
    }
  }

  Future<User?> getLoggedUser() async{
    final user = await _authRepository.getLoggedUser();
    return user;
  }

  logOut() async {
    await _authRepository.logout();
    isLoggedIn = false;
  }
}
