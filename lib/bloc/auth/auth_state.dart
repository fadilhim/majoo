part of 'auth_cubit.dart';

@immutable
abstract class AuthState {}

class AuthInitial extends AuthState {}

class LoginLoadInProgress extends AuthState {}

class LoginLoadSuccess extends AuthState {}

class LoginLoadFailure extends AuthState {
  final ErrorMessage error;

  LoginLoadFailure(this.error);
}

class RegisterLoadInProgress extends AuthState {}

class RegisterLoadSuccess extends AuthState {}

class RegisterLoadFailure extends AuthState {
  final ErrorMessage error;

  RegisterLoadFailure(this.error);
}