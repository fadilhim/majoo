import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:majoo/bloc/auth/auth_cubit.dart';
import 'package:majoo/main.dart';
import 'package:majoo/pages/splash_page.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: injector.get(),
      debugShowCheckedModeBanner: false,
      title: 'Majoo',
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        accentColor: Colors.white,
        primaryColor: Colors.blue,
        textTheme: GoogleFonts.nunitoTextTheme(),
        focusColor: Colors.blue,
        dialogBackgroundColor: Colors.white,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      darkTheme: ThemeData(
        primaryColor: Colors.blue,
        accentColor: Colors.white,
        textTheme: GoogleFonts.nunitoTextTheme(),
        focusColor: Colors.white,
        dialogBackgroundColor: const Color(0xff242424),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocBuilder<AuthCubit, AuthState>(builder: (context, state) {
        final isLoggedIn = context.read<AuthCubit>().isLoggedIn;
        return SplashPage(isLoggedIn: isLoggedIn);
      }),
    );
  }
}
